var dataRif = new Date();

var dataGiornoRif;
var dataMeseRif;
var dataAnnoRif;

calcolaDataNow(dataRif);

var lsStagioneSportiva;
var lsAnagraficaPalestre;
var lsAnagraficaPersone;
var lsAnagraficaCorsi;
var lsPersoneCorso;
var lsIscrizioneAnnuale;
var lsIscrizioneMensile;
var lsCertificatoMedicoAnnuale;
var lsPresenze;
var lsCategorie;

var periodoStagioneSportiva = "";
var stagioneSportivaAnnoInizio = " ";
var stagioneSportivaAnnoFine = " ";

var mostraPersoneDisattivate = "false";
var gestioneStatoPersona;

var idPalestraNomeRifCorsoDett;
var nomePalestraNomeRifCorsoDett;
var idCorsoNomeRifCorsoDett;
var nomeCorsoNomeRifCorsoDett;

var attiva_loadListViewPersoneCorso = "false";

var appSupportRootFolder = "Cartelle_App_Gestione_Palestra";
var appSupportBkFolder = appSupportRootFolder + "/Backup";
var appSupportDataSourceFolder = appSupportRootFolder + "/Sorgente_anagrafiche";
var appSupportReportFolder = appSupportRootFolder + "/Report";
var appSupportDichiarazioniFolder = appSupportRootFolder + "/Dichiarazioni";

// INIZIO document *************************************************************************
$(document).ready(function(event) {
  console.log("partito");

  $(".has-token-indicator").hide();
  var has_creds = false;
  if (localStorage.dropbox_access !== undefined) {
    var dbt = JSON.parse(localStorage.dropbox_access);
    if (dbt.access_token !== undefined) {
      $("#token").val(dbt.access_token);
      has_creds = true;
    }
  } else {
    // Da disattivare quando si fa la configurazione da Mac o Pc
    setLocalStorageSetAuth(); // 29/08/18 faccio scrivere sul local storage il valore del token che ho già inserito manualmente nel codice dopo averlo ottenuto con la procedura del sito da Mac o Pc
  }

  setupFileUpload();
  if (has_creds) {
    $(".btURLDropbox").removeClass("ui-disabled");
    hasCreds();
  } else {
    $(".btURLDropbox")
      .prop("disabled", true)
      .addClass("ui-disabled");
  }

  $.mobile.allowCrossDomainPages = true;
  $.mobile.ajaxEnabled = true;
  $.mobile.pushStateEnabled = false;
  $.support.cors = true;

  $("textarea").keypress(function(event) {
    if (event.keyCode == 13) {
      event.preventDefault();
    }
  });

  $("#loginOk").on("click", function() {
    $("#btGestioneDataMain").text(
      "Data di riferimento: " +
        dataGiornoRif +
        "/" +
        dataMeseRif +
        "/" +
        dataAnnoRif
    );
    $("#btGestioneDataDettaglioCorso").text(
      dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif
    );

    console.log(localStorage.stagioneSportiva);
    if (localStorage.stagioneSportiva !== undefined) {
      lsStagioneSportiva = JSON.parse(localStorage.stagioneSportiva);
      //console.log(lsStagioneSportiva);

      var annoInizio = lsStagioneSportiva.items[0].annoInizio;
      var annoFine = lsStagioneSportiva.items[0].annoFine;

      periodoStagioneSportiva = annoInizio + " - " + annoFine;
      $("#txtStagioneSportivaAnnoInizio").val(annoInizio);
      $("#txtStagioneSportivaAnnoFine").val(annoFine);
      $("#settingsStagioneSportiva").text(
        "Anno sportivo: " + periodoStagioneSportiva
      );
      $("#btSettingsStagioneSportivaDettaglioCorso").text(
        periodoStagioneSportiva
      );
      $.mobile.changePage("#mainPage");
    } else {
      console.log("Manca l'anno sportivo");
      $.mobile.changePage("#gestioneStagioneSportivaPage");
    }
  });

  // GESTIONE DATA DI RIFERIMENTO
  $("#btGestioneDataMain").on("click", function() {
    //alert(dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif);
    $("#txtDataGiorno").val(dataGiornoRif);
    $("#txtDataMese").val(dataMeseRif);
    $("#txtDataAnno").val(dataAnnoRif);
    $.mobile.changePage("#gestioneDataPage");
  });

  $("#btGestioneDataDettaglioCorso").on("click", function() {
    attiva_loadListViewPersoneCorso = "true";
    //alert(dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif);
    $("#txtDataGiorno").val(dataGiornoRif);
    $("#txtDataMese").val(dataMeseRif);
    $("#txtDataAnno").val(dataAnnoRif);
    $.mobile.changePage("#gestioneDataPage");
  });

  $("#btSettingsStagioneSportivaDettaglioCorso").on("click", function() {
    attiva_loadListViewPersoneCorso = "true";
    $.mobile.changePage("#gestioneStagioneSportivaPage");
  });

  $("#dataSalva").on("click", function() {
    var giornoMod = $("#txtDataGiorno").val();
    var meseMod = $("#txtDataMese").val();
    var annoMod = $("#txtDataAnno").val();
    calcolaDataMod(giornoMod, meseMod, annoMod);
    //$("#btGestioneDataMain .ui-btn-text").text("Data di riferimento: " + dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif);
    //$("#btGestioneDataDettaglioCorso .ui-btn-text").text("Data di riferimento: " + dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif);
    $("#btGestioneDataMain").text(
      "Data di riferimento: " +
        dataGiornoRif +
        "/" +
        dataMeseRif +
        "/" +
        dataAnnoRif
    );
    $("#btGestioneDataDettaglioCorso").text(
      dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif
    );
    if (attiva_loadListViewPersoneCorso == "true") {
      loadListViewPersoneCorso();
    }
    attiva_loadListViewPersoneCorso = "false";
    $.mobile.back();
  });

  $("#dataOggi").on("click", function() {
    calcolaDataNow(dataRif);
    $("#txtDataGiorno").val(dataGiornoRif);
    $("#txtDataMese").val(dataMeseRif);
    $("#txtDataAnno").val(dataAnnoRif);
    $("#btGestioneDataMain").text(
      "Data di riferimento: " +
        dataGiornoRif +
        "/" +
        dataMeseRif +
        "/" +
        dataAnnoRif
    );
    $("#btGestioneDataDettaglioCorso").text(
      dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif
    );
  });

  // GESTIONE SETTAGGI
  $("#settingsStagioneSportiva").on("click", function() {
    $.mobile.changePage("#gestioneStagioneSportivaPage");
  });

  $("#stagioneSportivaSalva").on("click", function() {
    var annoInizio = $("#txtStagioneSportivaAnnoInizio").val();
    var annoFine = $("#txtStagioneSportivaAnnoFine").val();
    if (annoInizio !== "" && annoFine !== "") {
      scriviLSStagioneSportiva(annoInizio, annoFine);

      if (attiva_loadListViewPersoneCorso == "true") {
        loadListViewPersoneCorso();
      }
      attiva_loadListViewPersoneCorso = "false";
      $.mobile.back();

      //$.mobile.changePage("#mainPage");
    } else {
      alert("I campi non posso essere vuoti!");
    }
  });

  $("#settingsAccountDropbox").on("click", function() {
    $.mobile.changePage("#gestioneAccountDropboxPage");
  });

  $("#debug").on("click", function() {
    $.mobile.changePage("#gestioneDebugPage");
  });

  //GESTIONE DEBUG
  $("#viewTabIscrizioneMensile").on("click", function() {});

  $("#viewTabPresenze").on("click", function() {});

  //GESTIONE CATEGORIE
  $("#settingsGestioneCategorie").on("click", function() {
    loadListViewCategorie();
    $.mobile.changePage("#gestioneCategoriePage");
  });

  $("#btGestioneCategorieAggiungi").on("click", function() {
    var x = "";
    var rowList = '<div class="ui-grid-d listaCategorieTableRow">';
    rowList +=
      '<div class="ui-block-a"><input id="txtCategoriaNome' +
      x +
      '"class="txtCategoriaNome" type="text" value=""/></div>' +
      '<div class="ui-block-b"><input id="txtCategoriaGrado' +
      x +
      '" class="txtCategoriaGrado" type="text" value=""/></div>' +
      '<div class="ui-block-c"><input id="txtCategoriaEtaMin' +
      x +
      '" class="txtCategoriaEta" type="text" value=""/></div>' +
      '<div class="ui-block-d"><input id="txtCategoriaEtaMax' +
      x +
      '" class="txtCategoriaEta" type="text" value=""/></div><div class="ui-block-e"><input type="button" class="btCatDelete" value="Elimina" /></div>';
    rowList += "</div>";
    $("#listaCategorie").append(rowList);
    $("#listaCategorie").listview("refresh");
  });

  $("#btGestioneCategorieSalva").on("click", function() {
    var arrCategorie = $("#listaCategorie").children();
    console.log("numero Categorie: " + arrCategorie.length);

    localStorage.categorie = '{"items":[]}';
    var arrObj = {
      items: []
    };

    for (var i = 0; i < arrCategorie.length; i++) {
      var riga = arrCategorie[i];

      var inputs = $(riga).find("input");

      if ($(inputs[0]).val() !== "") {
        var obj = {
          id: $(inputs[0]).val(),
          nome: $(inputs[0]).val(),
          grado: $(inputs[1]).val(),
          etaMin: $(inputs[2]).val(),
          etaMax: $(inputs[3]).val()
        };

        arrObj.items.push(obj);
      }
    }
    localStorage.categorie = JSON.stringify(arrObj);
    alert("Salvataggio completato!", "Messaggio");
  });

  // GESTIONE DATI (IMPORTAZIONE E CANCELLAZIONE)
  $("#settingsGestioneDati").on("click", function() {
    $.mobile.changePage("#gestioneDatiPage");
  });

  $("#btImportaAnagPalestre").on("click", function() {
    var file = {
      path: "/" + appSupportDataSourceFolder + "/anagraficaPalestre.csv",
      mime_type: "application/text"
    };
    $.dropbox.basic.getFileText(file).then(function(result) {
      console.dir(result);
      //console.dir(result.split("\r")[1]);

      var arrPalestre = result.split("\r");

      localStorage.anagraficaPalestre = '{"items":[]}';
      var arrObj = {
        items: []
      };
      for (var i = 1; i < arrPalestre.length; i++) {
        var item = arrPalestre[i];
        var itemDatas = item.split(";");

        if (itemDatas[6] !== "") {
          var obj = {
            id: itemDatas[6],
            nome: itemDatas[0],
            via: itemDatas[1],
            cap: itemDatas[2],
            citta: itemDatas[3],
            provincia: itemDatas[4],
            nazione: itemDatas[5],
            codiceFiscale: itemDatas[6]
          };

          arrObj.items.push(obj);
        }
      }
      localStorage.anagraficaPalestre = JSON.stringify(arrObj);
      alert("Importazone anagrafica palestre conclusa!");
    });
  });

  $("#btImportaAnagPersone").on("click", function() {
    var file = {
      path: "/" + appSupportDataSourceFolder + "/anagraficaPersone.csv",
      mime_type: "application/text"
    };
    $.dropbox.basic.getFileText(file).then(function(result) {
      console.dir(result);
      //console.dir(result.split("\r")[1]);

      var arrPersone = result.split("\r");

      localStorage.anagraficaPersone = '{"items":[]}';
      var arrObj = {
        items: []
      };
      for (var i = 1; i < arrPersone.length; i++) {
        var item = arrPersone[i];
        var itemDatas = item.split(";");

        if (itemDatas[1] !== "" && itemDatas[6] !== "") {
          var obj = {
            id: itemDatas[1] + "-" + itemDatas[6],
            idPersona: itemDatas[6],
            idPalestra: itemDatas[1],
            nomePalestra: itemDatas[0],
            nome: itemDatas[2],
            cognome: itemDatas[3],
            dataNascita: itemDatas[4],
            luogoNascita: itemDatas[5],
            codiceFiscale: itemDatas[6],
            via: itemDatas[7],
            cap: itemDatas[8],
            citta: itemDatas[9],
            provincia: itemDatas[10],
            nazione: itemDatas[11],
            telefono: itemDatas[12],
            cellulare: itemDatas[13],
            email: itemDatas[14],
            nomeGenitore: itemDatas[15],
            cognomeGenitore: itemDatas[16],
            codiceFiscaleGenitore: itemDatas[17],
            dataUltimoEsame: itemDatas[18],
            grado: itemDatas[19],
            nota: itemDatas[20],
            stato: itemDatas[21]
          };

          arrObj.items.push(obj);
        }
      }
      localStorage.anagraficaPersone = JSON.stringify(arrObj);
      alert("Importazone anagrafica persone conclusa!");
    });
  });

  $("#btImportaRangeCategorie").on("click", function() {
    var file = {
      path: "/" + appSupportDataSourceFolder + "/categorie.csv",
      mime_type: "application/text"
    };
    $.dropbox.basic.getFileText(file).then(function(result) {
      console.dir(result);
      //console.dir(result.split("\r")[1]);

      var arrCategorie = result.split("\r");

      localStorage.categorie = '{"items":[]}';
      var arrObj = {
        items: []
      };
      for (var i = 1; i < arrCategorie.length; i++) {
        var item = arrCategorie[i];
        var itemDatas = item.split(";");

        if (itemDatas[0] !== "") {
          var obj = {
            id: itemDatas[0],
            nome: itemDatas[0],
            grado: itemDatas[1],
            etaMin: itemDatas[2],
            etaMax: itemDatas[3]
          };

          arrObj.items.push(obj);
        }
      }
      localStorage.categorie = JSON.stringify(arrObj);
      alert("Importazone categorie conclusa!");
    });
  });

  $("#btCancellaTuttiDati").on("click", function() {
    var mess = confirm("Confermi l'eliminazione di tutti i dati?");
    if (mess) {
      localStorage.anagraficaPalestre = '{"items":[]}';
      localStorage.anagraficaCorsi = '{"items":[]}';
      localStorage.anagraficaPersone = '{"items":[]}';
      localStorage.personeCorso = '{"items":[]}';
      localStorage.iscrizioneAnnuale = '{"items":[]}';
      localStorage.iscrizioneMensile = '{"items":[]}';
      localStorage.certificatoMedicoAnnuale = '{"items":[]}';
      localStorage.presenze = '{"items":[]}';
      //localStorage.stagioneSportiva = '{"items":[]}';

      alert("Eliminazione dati conclusa con successo!");
    }
  });

  $("#btCancellaDatiPerRangeData").on("click", function() {
    $.mobile.changePage("#cancellaDatiPerRangeDataPage");
  });

  $("#btCancellaIscrizioneAnnuale").on("click", function() {
    var inizioCancellaDati = $("#txtCancellaDatiAnnoInizio").val();
    var fineCancellaDati = $("#txtCancellaDatiAnnoFine").val();
    var stagioneSportivaRif = inizioCancellaDati + " - " + fineCancellaDati;

    if (inizioCancellaDati !== "" && fineCancellaDati !== "") {
      if (localStorage.iscrizioneAnnuale === undefined) {
        localStorage.iscrizioneAnnuale = '{"items":[]}';
      }
      lsIscrizioneAnnuale = JSON.parse(localStorage.iscrizioneAnnuale);

      for (var i = lsIscrizioneAnnuale.items.length - 1; i > -1; i--) {
        var item = lsIscrizioneAnnuale.items[i];
        if (item.stagioneSportiva === stagioneSportivaRif) {
          lsIscrizioneAnnuale.items.splice(i, 1);
          console.log("Elimino le iscrizioni annuali");
        }
      }
      localStorage.iscrizioneAnnuale = JSON.stringify(lsIscrizioneAnnuale);
      alert("Eliminazione iscrizioni annuali conclusa con successo!");
    } else {
      alert("I campi non posso essere vuoti!");
    }
  });

  $("#btCancellaCertificatoMedicoAnnuale").on("click", function() {
    var inizioCancellaDati = $("#txtCancellaDatiAnnoInizio").val();
    var fineCancellaDati = $("#txtCancellaDatiAnnoFine").val();
    var stagioneSportivaRif = inizioCancellaDati + " - " + fineCancellaDati;

    if (inizioCancellaDati !== "" && fineCancellaDati !== "") {
      if (localStorage.certificatoMedicoAnnuale === undefined) {
        localStorage.certificatoMedicoAnnuale = '{"items":[]}';
      }
      lsCertificatoMedicoAnnuale = JSON.parse(
        localStorage.certificatoMedicoAnnuale
      );

      for (var i = lsCertificatoMedicoAnnuale.items.length - 1; i > -1; i--) {
        var item = lsCertificatoMedicoAnnuale.items[i];
        if (item.stagioneSportiva === stagioneSportivaRif) {
          lsCertificatoMedicoAnnuale.items.splice(i, 1);
          console.log("Elimino i certificati");
        }
      }
      localStorage.certificatoMedicoAnnuale = JSON.stringify(
        lsCertificatoMedicoAnnuale
      );
      alert("Eliminazione certificati medici conclusa con successo!");
    } else {
      alert("I campi non posso essere vuoti!");
    }
  });

  $("#btCancellaIscrizioneMensile").on("click", function() {
    var inizioCancellaDati = $("#txtCancellaDatiAnnoInizio").val();
    var fineCancellaDati = $("#txtCancellaDatiAnnoFine").val();
    var stagioneSportivaRif = inizioCancellaDati + " - " + fineCancellaDati;

    if (inizioCancellaDati !== "" && fineCancellaDati !== "") {
      if (localStorage.iscrizioneMensile === undefined) {
        localStorage.iscrizioneMensile = '{"items":[]}';
      }
      lsIscrizioneMensile = JSON.parse(localStorage.iscrizioneMensile);

      for (var i = lsIscrizioneMensile.items.length - 1; i > -1; i--) {
        var item = lsIscrizioneMensile.items[i];
        if (item.stagioneSportiva === stagioneSportivaRif) {
          lsIscrizioneMensile.items.splice(i, 1);
          console.log("Elimino le iscrizioni mensili");
        }
      }
      localStorage.iscrizioneMensile = JSON.stringify(lsIscrizioneMensile);
      alert("Eliminazione iscrizioni mensili conclusa con successo!");
    } else {
      alert("I campi non posso essere vuoti!");
    }
  });

  $("#btCancellaPresenze").on("click", function() {
    var inizioCancellaDati = $("#txtCancellaDatiAnnoInizio").val();
    var fineCancellaDati = $("#txtCancellaDatiAnnoFine").val();
    var stagioneSportivaRif = inizioCancellaDati + " - " + fineCancellaDati;

    if (inizioCancellaDati !== "" && fineCancellaDati !== "") {
      if (localStorage.presenze === undefined) {
        localStorage.presenze = '{"items":[]}';
      }
      lsPresenze = JSON.parse(localStorage.presenze);

      for (var i = lsPresenze.items.length - 1; i > -1; i--) {
        var item = lsPresenze.items[i];
        if (item.stagioneSportiva === stagioneSportivaRif) {
          lsPresenze.items.splice(i, 1);
          console.log("Elimino le presenze");
        }
      }
      localStorage.presenze = JSON.stringify(lsPresenze);
      alert("Eliminazione presenze conclusa con successo!");
    } else {
      alert("I campi non posso essere vuoti!");
    }
  });

  // GESTIONE BACKUP DATI
  $("#settingsGestioneBackup").on("click", function() {
    $.mobile.changePage("#gestioneBackupPage");
  });

  $("#btEseguiBackup").on("click", function() {
    var mess = confirm("Confermi l'avvio del backup?");
    if (mess) {
      var startBackup = function() {
        var execBackup = function(ls, fileName) {
          // setTimeout(function (ev) {
          var filePath = appSupportBkFolder + "/" + fileName;
          var contents = String(ls);

          var prom = Q($.dropbox.basic.addFileText(filePath, contents));
          console.log(filePath);

          return prom;
          // }, 2000);
        };

        //*** INIZIO *************
        var promises = [];

        promises.push(
          execBackup(
            JSON.stringify(lsStagioneSportiva),
            "stagioneSportiva.json"
          )
        );

        lsAnagraficaPalestre = JSON.parse(localStorage.anagraficaPalestre);
        promises.push(
          execBackup(
            JSON.stringify(lsAnagraficaPalestre),
            "anagraficaPalestre.json"
          )
        );

        lsAnagraficaPersone = JSON.parse(localStorage.anagraficaPersone);
        promises.push(
          execBackup(
            JSON.stringify(lsAnagraficaPersone),
            "anagraficaPersone.json"
          )
        );

        lsAnagraficaCorsi = JSON.parse(localStorage.anagraficaCorsi);
        promises.push(
          execBackup(JSON.stringify(lsAnagraficaCorsi), "anagraficaCorsi.json")
        );

        lsPersoneCorso = JSON.parse(localStorage.personeCorso);
        promises.push(
          execBackup(JSON.stringify(lsPersoneCorso), "personeCorso.json")
        );

        lsIscrizioneAnnuale = JSON.parse(localStorage.iscrizioneAnnuale);
        promises.push(
          execBackup(
            JSON.stringify(lsIscrizioneAnnuale),
            "iscrizioneAnnuale.json"
          )
        );

        lsIscrizioneMensile = JSON.parse(localStorage.iscrizioneMensile);
        promises.push(
          execBackup(
            JSON.stringify(lsIscrizioneMensile),
            "iscrizioneMensile.json"
          )
        );

        lsCertificatoMedicoAnnuale = JSON.parse(
          localStorage.certificatoMedicoAnnuale
        );
        promises.push(
          execBackup(
            JSON.stringify(lsCertificatoMedicoAnnuale),
            "certificatoMedicoAnnuale.json"
          )
        );

        lsPresenze = JSON.parse(localStorage.presenze);
        promises.push(execBackup(JSON.stringify(lsPresenze), "presenze.json"));

        lsCategorie = JSON.parse(localStorage.categorie);
        promises.push(
          execBackup(JSON.stringify(lsCategorie), "categorie.json")
        );

        return Q.allSettled(promises).then(function() {
          console.log("*** FINE BACKUP ***");
          return data.length ? data[0] : [];
        });
        //***********************
      };

      return startBackup()
        .then(function(data) {
          console.log(data);
          return data;
        })
        .catch(function(err) {
          //debugger;
          //console.log(err);
        })
        .finally(function() {
          console.log("Fine backup");
          alert("Backup concluso!");
        });
    }
  });

  $("#btRipristinaBackup").on("click", function() {
    var mess = confirm("Confermi il ripristino del backup?");
    if (mess) {
      var startImportBackup = function() {
        var importBackupStagioneSportiva = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsStagioneSportiva = JSON.parse(result);
              console.log(lsStagioneSportiva.items);
              if (lsStagioneSportiva !== undefined) {
                var annoInizio = lsStagioneSportiva.items[0].annoInizio;
                var annoFine = lsStagioneSportiva.items[0].annoFine;
                scriviLSStagioneSportiva(annoInizio, annoFine);
              }
              return result;
            })
          );

          return prom;
        };

        var importBackupAnagPalestre = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsAnagraficaPalestre = JSON.parse(result);
              console.log(lsAnagraficaPalestre.items);
              console.log(lsAnagraficaPalestre.items.length);
              localStorage.anagraficaPalestre = '{"items":[]}';
              var arrObj = {
                items: []
              };
              for (var i = 0; i < lsAnagraficaPalestre.items.length; i++) {
                var item = lsAnagraficaPalestre.items[i];

                var obj = {
                  id: item.id,
                  nome: item.nome,
                  via: item.via,
                  cap: item.cap,
                  citta: item.citta,
                  provincia: item.provincia,
                  nazione: item.nazione,
                  codiceFiscale: item.codiceFiscale
                };

                arrObj.items.push(obj);
              }
              localStorage.anagraficaPalestre = JSON.stringify(arrObj);
              return result;
            })
          );

          return prom;
        };

        var importBackupAnagPersone = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsAnagraficaPersone = JSON.parse(result);
              console.log(lsAnagraficaPersone.items);
              console.log(lsAnagraficaPersone.items.length);
              localStorage.anagraficaPersone = '{"items":[]}';
              var arrObj = {
                items: []
              };
              for (var i = 0; i < lsAnagraficaPersone.items.length; i++) {
                var item = lsAnagraficaPersone.items[i];

                var obj = {
                  id: item.id,
                  idPersona: item.idPersona,
                  idPalestra: item.idPalestra,
                  nomePalestra: item.nomePalestra,
                  nome: item.nome,
                  cognome: item.cognome,
                  dataNascita: item.dataNascita,
                  luogoNascita: item.luogoNascita,
                  codiceFiscale: item.codiceFiscale,
                  via: item.via,
                  cap: item.cap,
                  citta: item.citta,
                  provincia: item.provincia,
                  nazione: item.nazione,
                  telefono: item.telefono,
                  cellulare: item.cellulare,
                  email: item.email,
                  nomeGenitore: item.nomeGenitore,
                  cognomeGenitore: item.cognomeGenitore,
                  codiceFiscaleGenitore: item.codiceFiscaleGenitore,
                  dataUltimoEsame: item.dataUltimoEsame,
                  grado: item.grado,
                  nota: item.nota,
                  stato: item.stato
                };

                arrObj.items.push(obj);
              }
              localStorage.anagraficaPersone = JSON.stringify(arrObj);
              return result;
            })
          );

          return prom;
        };

        var importBackupAnagCorsi = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsAnagraficaCorsi = JSON.parse(result);
              console.log(lsAnagraficaCorsi.items);
              console.log(lsAnagraficaCorsi.items.length);
              localStorage.anagraficaCorsi = '{"items":[]}';
              var arrObj = {
                items: []
              };
              for (var i = 0; i < lsAnagraficaCorsi.items.length; i++) {
                var item = lsAnagraficaCorsi.items[i];

                var obj = {
                  id: item.id,
                  idCorso: item.idCorso,
                  idPalestra: item.idPalestra,
                  nomePalestra: item.nomePalestra,
                  nome: item.nome,
                  descrizione: item.descrizione,
                  costoMensileNumeri: item.costoMensileNumeri,
                  costoMensileLettere: item.costoMensileLettere,
                  nota: item.nota
                };

                arrObj.items.push(obj);
              }
              localStorage.anagraficaCorsi = JSON.stringify(arrObj);
              return result;
            })
          );

          return prom;
        };

        var importBackupPersoneCorso = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsPersoneCorso = JSON.parse(result);
              console.log(lsPersoneCorso.items);
              console.log(lsPersoneCorso.items.length);
              localStorage.personeCorso = '{"items":[]}';
              var arrObj = {
                items: []
              };
              for (var i = 0; i < lsPersoneCorso.items.length; i++) {
                var item = lsPersoneCorso.items[i];

                var obj = {
                  id: item.id,
                  idPersona: item.idPersona,
                  idPalesttra: item.idPalestra
                };

                arrObj.items.push(obj);
              }
              localStorage.personeCorso = JSON.stringify(arrObj);
              return result;
            })
          );

          return prom;
        };

        var importBackupIscrizioneAnnuale = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsIscrizioneAnnuale = JSON.parse(result);
              console.log(lsIscrizioneAnnuale.items);
              console.log(lsIscrizioneAnnuale.items.length);
              localStorage.iscrizioneAnnuale = '{"items":[]}';
              var arrObj = {
                items: []
              };
              for (var i = 0; i < lsIscrizioneAnnuale.items.length; i++) {
                var item = lsIscrizioneAnnuale.items[i];

                var obj = {
                  idCorso: item.idCorso,
                  dataPagamento: item.dataPagamento,
                  idMese: item.idMese,
                  idAnno: item.idAnno,
                  idPersona: item.idPersona,
                  idPalestra: item.idPalestra,
                  stagioneSportiva: item.stagioneSportiva
                };

                arrObj.items.push(obj);
              }
              localStorage.iscrizioneAnnuale = JSON.stringify(arrObj);
              return result;
            })
          );

          return prom;
        };

        var importBackupIscrizioneMensile = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsIscrizioneMensile = JSON.parse(result);
              console.log(lsIscrizioneMensile.items);
              console.log(lsIscrizioneMensile.items.length);
              localStorage.iscrizioneMensile = '{"items":[]}';
              var arrObj = {
                items: []
              };
              for (var i = 0; i < lsIscrizioneMensile.items.length; i++) {
                var item = lsIscrizioneMensile.items[i];

                var obj = {
                  idCorso: item.idCorso,
                  dataPagamento: item.dataPagamento,
                  idMese: item.idMese,
                  idAnno: item.idAnno,
                  idPersona: item.idPersona,
                  idPalestra: item.idPalestra,
                  stagioneSportiva: item.stagioneSportiva
                };

                arrObj.items.push(obj);
              }
              localStorage.iscrizioneMensile = JSON.stringify(arrObj);
              return result;
            })
          );

          return prom;
        };

        var importBackupCertificatoMedicoAnnuale = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsCertificatoMedicoAnnuale = JSON.parse(result);
              console.log(lsCertificatoMedicoAnnuale.items);
              console.log(lsCertificatoMedicoAnnuale.items.length);
              localStorage.certificatoMedicoAnnuale = '{"items":[]}';
              var arrObj = {
                items: []
              };
              for (
                var i = 0;
                i < lsCertificatoMedicoAnnuale.items.length;
                i++
              ) {
                var item = lsCertificatoMedicoAnnuale.items[i];

                var obj = {
                  idCorso: item.idCorso,
                  dataPagamento: item.dataPagamento,
                  idMese: item.idMese,
                  idAnno: item.idAnno,
                  idPersona: item.idPersona,
                  idPalestra: item.idPalestra,
                  stagioneSportiva: item.stagioneSportiva
                };

                arrObj.items.push(obj);
              }
              localStorage.certificatoMedicoAnnuale = JSON.stringify(arrObj);
              return result;
            })
          );

          return prom;
        };

        var importBackupPresenze = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsPresenze = JSON.parse(result);
              console.log(lsPresenze.items);
              console.log(lsPresenze.items.length);
              localStorage.presenze = '{"items":[]}';
              var arrObj = {
                items: []
              };
              for (var i = 0; i < lsPresenze.items.length; i++) {
                var item = lsPresenze.items[i];

                var obj = {
                  idCorso: item.idCorso,
                  dataPresenza: item.dataPresenza,
                  idGiorno: item.idGiorno,
                  idMese: item.idMese,
                  idAnno: item.idAnno,
                  idPersona: item.idPersona,
                  idPalestra: item.idPalestra,
                  stagioneSportiva: item.stagioneSportiva
                };

                arrObj.items.push(obj);
              }
              localStorage.presenze = JSON.stringify(arrObj);
              return result;
            })
          );

          return prom;
        };

        var importBackupCategorie = function(lsName) {
          console.log(appSupportBkFolder + "/" + lsName);
          var file = {
            path: "/" + appSupportBkFolder + "/" + lsName,
            mime_type: "application/json"
          };
          var prom = Q(
            $.dropbox.basic.getFileText(file).then(function(result) {
              console.dir(result);

              lsCategorie = JSON.parse(result);
              console.log(lsCategorie.items);
              console.log(lsCategorie.items.length);
              localStorage.categorie = '{"items":[]}';
              var arrObj = {
                items: []
              };
              for (var i = 0; i < lsCategorie.items.length; i++) {
                var item = lsCategorie.items[i];

                var obj = {
                  id: item.id,
                  nome: item.nome,
                  etaMin: item.etaMin,
                  etaMax: item.etaMax,
                  grado: item.grado
                };

                arrObj.items.push(obj);
              }
              localStorage.categorie = JSON.stringify(arrObj);
              return result;
            })
          );

          return prom;
        };

        //*** INIZIO *************
        var promises = [];

        promises.push(importBackupStagioneSportiva("stagioneSportiva.json"));
        promises.push(importBackupAnagPalestre("anagraficaPalestre.json"));
        promises.push(importBackupAnagPersone("anagraficaPersone.json"));
        promises.push(importBackupAnagCorsi("anagraficaCorsi.json"));
        promises.push(importBackupPersoneCorso("personeCorso.json"));
        promises.push(importBackupIscrizioneAnnuale("iscrizioneAnnuale.json"));
        promises.push(importBackupIscrizioneMensile("iscrizioneMensile.json"));
        promises.push(
          importBackupCertificatoMedicoAnnuale("certificatoMedicoAnnuale.json")
        );
        promises.push(importBackupPresenze("presenze.json"));
        promises.push(importBackupCategorie("categorie.json"));

        return Q.allSettled(promises).then(function() {
          console.log("*** FINE IMPORT BACKUP ***");
          return data.length ? data[0] : [];
        });
        //***********************
      };

      return startImportBackup()
        .then(function(data) {
          console.log(data);
          return data;
        })
        .catch(function(err) {
          //debugger;
          //console.log(err);
        })
        .finally(function() {
          console.log("Fine ripristino backup");
          alert("Ripristino backup concluso!");
        });
    }
  });

  // PALESTRE
  $("#settingsPalestre").on("click", function() {
    if (localStorage.anagraficaPalestre === undefined) {
      localStorage.anagraficaPalestre = '{"items":[]}';
    }
    lsAnagraficaPalestre = JSON.parse(localStorage.anagraficaPalestre);
    console.log(lsAnagraficaPalestre);
    loadListViewPalestre();
    $.mobile.changePage("#gestionePalestrePage");
  });

  $("#btGestionePalestreAggiungi").on("click", function() {
    $("#dettaglioPalestraPage .resetta").val(""); // RESETTA

    $.mobile.changePage("#dettaglioPalestraPage");
  });

  $("#btDettaglioPalestraSalva").on("click", function() {
    var nomeNuovaPalestra = $("#txtPalestraNome").val();
    var cfNuovaPalestra = $("#txtPalestraCodiceFiscale").val();
    if (nomeNuovaPalestra !== "" && cfNuovaPalestra !== "") {
      if (localStorage.anagraficaPalestre === undefined) {
        localStorage.anagraficaPalestre = '{"items":[]}';
      }
      lsAnagraficaPalestre = JSON.parse(localStorage.anagraficaPalestre);

      var obj = {
        id: cfNuovaPalestra,
        nome: nomeNuovaPalestra,
        via: $("#txtPalestraVia").val(),
        cap: $("#txtPalestraCap").val(),
        citta: $("#txtPalestraCitta").val(),
        provincia: $("#txtPalestraProvincia").val(),
        nazione: $("#txtPalestraNazione").val(),
        codiceFiscale: $("#txtPalestraCodiceFiscale").val()
      };
      $("#txtPalestraNome").data("id", obj.id);

      var addItem = ckExistsItem(lsAnagraficaPalestre, obj.id);
      if (addItem) {
        lsAnagraficaPalestre.items.push(obj);
        localStorage.anagraficaPalestre = JSON.stringify(lsAnagraficaPalestre);
        alert("Salvataggio completato!", "Messaggio");
      } else {
        var s =
          "La palestra '" +
          nomeNuovaPalestra +
          "' esiste gia'! Vuoi sovrascriverla?";
        var mess = confirm(s, false, "Message");
        if (mess) {
          for (var i = lsAnagraficaPalestre.items.length - 1; i > -1; i--) {
            var item = lsAnagraficaPalestre.items[i];
            //if(item.id === nomeNuovaPalestra){
            if (item.id === obj.id) {
              lsAnagraficaPalestre.items.splice(i, 1);
            }
          }
          lsAnagraficaPalestre.items.push(obj);
          localStorage.anagraficaPalestre = JSON.stringify(
            lsAnagraficaPalestre
          );
        } else {
          alert("Salvataggio interrotto!", "Attenzione");
        }
      }
    } else {
      alert("Campo 'Nome Palestra' vuoto!");
    }
    loadListViewPalestre();
  });

  $("#btDettaglioPalestraElimina").on("click", function() {
    var nomePalestra = $("#txtPalestraNome").val();
    var id = $("#txtPalestraNome").data("id");
    console.log(id);
    lsAnagraficaPalestre = JSON.parse(localStorage.anagraficaPalestre);

    if (nomePalestra !== "") {
      var remove = false;

      // Lodash
      var indexVal = _.findIndex(lsAnagraficaPalestre.items, function(item) {
        return item.id === id;
      });
      if (indexVal !== -1) {
        var s = "Confermi l'eliminazione?";
        var mess = confirm(s, false, "Message");
        if (mess) {
          remove = true;
          lsAnagraficaPalestre.items.splice(indexVal, 1);
          localStorage.anagraficaPalestre = JSON.stringify(
            lsAnagraficaPalestre
          );
          console.log("Elimino la palestra " + id);

          if (localStorage.anagraficaCorsi === undefined) {
            localStorage.anagraficaCorsi = '{"items":[]}';
          }
          lsAnagraficaCorsi = JSON.parse(localStorage.anagraficaCorsi);

          for (var i = lsAnagraficaCorsi.items.length - 1; i > -1; i--) {
            var item = lsAnagraficaCorsi.items[i];
            if (item.idPalestra === id) {
              lsAnagraficaCorsi.items.splice(i, 1);
              console.log("Elimino i corsi " + item.id);
            }
          }
          localStorage.anagraficaCorsi = JSON.stringify(lsAnagraficaCorsi);

          if (localStorage.anagraficaPersone === undefined) {
            localStorage.anagraficaPersone = '{"items":[]}';
          }
          lsAnagraficaPersone = JSON.parse(localStorage.anagraficaPersone);

          for (var i = lsAnagraficaPersone.items.length - 1; i > -1; i--) {
            var item = lsAnagraficaPersone.items[i];
            if (item.idPalestra === id) {
              lsAnagraficaPersone.items.splice(i, 1);
              console.log("Elimino le persone " + item.id);
            }
          }
          localStorage.anagraficaPersone = JSON.stringify(lsAnagraficaPersone);

          if (localStorage.personeCorso === undefined) {
            localStorage.personeCorso = '{"items":[]}';
          }
          lsPersoneCorso = JSON.parse(localStorage.personeCorso);

          for (var i = lsPersoneCorso.items.length - 1; i > -1; i--) {
            var item = lsPersoneCorso.items[i];
            if (item.idPalestra === id) {
              lsPersoneCorso.items.splice(i, 1);
              console.log(
                "Elimino l'abbinamento ai corsi della palestra " +
                  item.idPersona
              );
            }
          }
          localStorage.personeCorso = JSON.stringify(lsPersoneCorso);

          if (localStorage.iscrizioneAnnuale === undefined) {
            localStorage.iscrizioneAnnuale = '{"items":[]}';
          }
          lsIscrizioneAnnuale = JSON.parse(localStorage.iscrizioneAnnuale);

          for (var i = lsIscrizioneAnnuale.items.length - 1; i > -1; i--) {
            var item = lsIscrizioneAnnuale.items[i];
            if (item.idPalestra === id) {
              lsIscrizioneAnnuale.items.splice(i, 1);
              console.log("Elimino l'iscrizione annuale di " + item.idPersona);
            }
          }
          localStorage.iscrizioneAnnuale = JSON.stringify(lsIscrizioneAnnuale);

          if (localStorage.iscrizioneMensile === undefined) {
            localStorage.iscrizioneMensile = '{"items":[]}';
          }
          lsIscrizioneMensile = JSON.parse(localStorage.iscrizioneMensile);

          for (var i = lsIscrizioneMensile.items.length - 1; i > -1; i--) {
            var item = lsIscrizioneMensile.items[i];
            if (item.idPalestra === id) {
              lsIscrizioneMensile.items.splice(i, 1);
              console.log("Elimino le iscrizioni mensili di " + item.idPersona);
            }
          }
          localStorage.iscrizioneMensile = JSON.stringify(lsIscrizioneMensile);

          if (localStorage.certificatoMedicoAnnuale === undefined) {
            localStorage.certificatoMedicoAnnuale = '{"items":[]}';
          }
          lsCertificatoMedicoAnnuale = JSON.parse(
            localStorage.certificatoMedicoAnnuale
          );

          for (
            var i = lsCertificatoMedicoAnnuale.items.length - 1;
            i > -1;
            i--
          ) {
            var item = lsCertificatoMedicoAnnuale.items[i];
            if (item.idPalestra === id) {
              lsCertificatoMedicoAnnuale.items.splice(i, 1);
              console.log("Elimino il certificato di " + item.idPersona);
            }
          }
          localStorage.certificatoMedicoAnnuale = JSON.stringify(
            lsCertificatoMedicoAnnuale
          );

          if (localStorage.presenze === undefined) {
            localStorage.presenze = '{"items":[]}';
          }
          lsPresenze = JSON.parse(localStorage.presenze);

          for (var i = lsPresenze.items.length - 1; i > -1; i--) {
            var item = lsPresenze.items[i];
            if (item.idPalestra === id) {
              lsPresenze.items.splice(i, 1);
              console.log("Elimino le presenze di " + item.idPersona);
            }
          }
          localStorage.presenze = JSON.stringify(lsPresenze);
        }
      }
      if (!remove) {
        alert("La Palestra da cancellare non e' presente!", "Attenzione");
      }

      $.mobile.changePage("#gestionePalestrePage");
    } else {
      alert("Campo 'Nome Palestra' vuoto!");
    }
    loadListViewPalestre();
  });

  // PERSONE

  $("#btGestionePersone").on("click", function() {
    // Recupera i dati dalla pagina dettaglio palestra
    var nomePalestraRif = $("#txtPalestraNome").val();
    var idPalestraRif = $("#txtPalestraNome").data("id");

    // Inserisce i dati nella pagina gestione persone
    $("#txtPalestraNomeRif").val(nomePalestraRif);
    $("#txtPalestraNomeRif").data("id", idPalestraRif);
    //console.log($("#txtPalestraNomeRif").val() + " - " + $("#txtPalestraNomeRif").data("id"));

    if (mostraPersoneDisattivate == "false") {
      $("#btGestionePersoneFiltra").text("Mostra iscritti disattivati");
    } else {
      $("#btGestionePersoneFiltra").text("Nascondi iscritti disattivati");
    }

    loadListViewPersone();
    $.mobile.changePage("#gestionePersonePage");
  });

  // Filtro persone disattivate lista iscritti
  $("#btGestionePersoneFiltra").on("click", function(e) {
    e.preventDefault();
    //$("#btGestionePersoneFiltra").css({"top":"1px","height":"29px","padding-top":"10px","width":"240px"});
    if (mostraPersoneDisattivate == "false") {
      mostraPersoneDisattivate = "true";
      $("#btGestionePersoneFiltra").text("Nascondi iscritti disattivati");
      //alert(mostraPersoneDisattivate);
    } else {
      mostraPersoneDisattivate = "false";
      $("#btGestionePersoneFiltra").text("Mostra iscritti disattivati");
      //alert(mostraPersoneDisattivate);
    }
    loadListViewPersone();
  });

  // Filtro persone disattivate lista iscritti dettaglio corso
  $("#btDettaglioCorsoFiltra").on("click", function(e) {
    e.preventDefault();
    //$("#btDettaglioCorsoFiltra").css({"top":"1px","height":"29px","padding-top":"10px","width":"240px"});
    if (mostraPersoneDisattivate == "false") {
      mostraPersoneDisattivate = "true";
      $("#btDettaglioCorsoFiltra").text("Nascondi iscritti disattivati");
      //alert(mostraPersoneDisattivate);
    } else {
      mostraPersoneDisattivate = "false";
      $("#btDettaglioCorsoFiltra").text("Mostra iscritti disattivati");
      //alert(mostraPersoneDisattivate);
    }
    loadListViewPersoneCorso();
  });

  // Filtro persone disattivate lista iscritti dettaglio corso persone aggiungi
  $("#btPersoneCorsoAggFiltra").on("click", function(e) {
    e.preventDefault();
    //$("#btPersoneCorsoAggFiltra").css({"top":"1px","height":"29px","padding-top":"10px","width":"240px"});
    if (mostraPersoneDisattivate == "false") {
      mostraPersoneDisattivate = "true";
      $("#btPersoneCorsoAggFiltra").text("Nascondi iscritti disattivati");
      //alert(mostraPersoneDisattivate);
    } else {
      mostraPersoneDisattivate = "false";
      $("#btPersoneCorsoAggFiltra").text("Mostra iscritti disattivati");
      //alert(mostraPersoneDisattivate);
    }
    loadListViewPersoneCorsoAgg();
  });

  $("#btGestionePersoneAggiungiSopra").on("click", aggiungiPersona);
  $("#btGestionePersoneAggiungiSotto").on("click", aggiungiPersona);

  $("#btDettaglioPersonaSalva").on("click", function() {
    var nomePalestraRifDett = $("#txtPalestraNomeRifDett").val();
    var idPalestraRifDett = $("#txtPalestraNomeRifDett").data("id");
    var nomeNuovaPersona = $("#txtPersonaNome").val();
    var cognomeNuovaPersona = $("#txtPersonaCognome").val();
    var cfNuovaPersona = $("#txtPersonaCodiceFiscale").val();
    //console.log(nomePalestraRifDett + " > " + idPalestraRifDett);

    if (
      nomeNuovaPersona !== "" &&
      cognomeNuovaPersona !== "" &&
      cfNuovaPersona !== ""
    ) {
      if (localStorage.anagraficaPersone === undefined) {
        localStorage.anagraficaPersone = '{"items":[]}';
      }
      lsAnagraficaPersone = JSON.parse(localStorage.anagraficaPersone);

      var obj = {
        id: idPalestraRifDett + "-" + cfNuovaPersona,
        idPersona: cfNuovaPersona,
        idPalestra: idPalestraRifDett,
        nomePalestra: nomePalestraRifDett,
        nome: nomeNuovaPersona,
        cognome: cognomeNuovaPersona,
        dataNascita: $("#txtPersonaDataNascita").val(),
        luogoNascita: $("#txtPersonaLuogoNascita").val(),
        codiceFiscale: cfNuovaPersona,
        via: $("#txtPersonaIndirizzoVia").val(),
        cap: $("#txtPersonaIndirizzoCap").val(),
        citta: $("#txtPersonaIndirizzoCitta").val(),
        provincia: $("#txtPersonaIndirizzoProvincia").val(),
        nazione: $("#txtPersonaIndirizzoNazione").val(),
        telefono: $("#txtPersonaTelefono").val(),
        cellulare: $("#txtPersonaCellulare").val(),
        email: $("#txtPersonaEmail").val(),
        nomeGenitore: $("#txtPersonaGenitoreNome").val(),
        cognomeGenitore: $("#txtPersonaGenitoreCognome").val(),
        codiceFiscaleGenitore: $("#txtPersonaGenitoreCodiceFiscale").val(),
        dataUltimoEsame: $("#txtPersonaDataUltimoEsame").val(),
        grado: $("#txtPersonaGrado").val(),
        nota: $("#txtPersonaNota").val(),
        stato: gestioneStatoPersona
      };

      var addItem = ckExistsItem(lsAnagraficaPersone, obj.id);
      if (addItem) {
        lsAnagraficaPersone.items.push(obj);
        localStorage.anagraficaPersone = JSON.stringify(lsAnagraficaPersone);
        alert("Salvataggio completato!", "Messaggio");
      } else {
        var s =
          "Nella palestra '" +
          nomePalestraRifDett +
          "'\n'" +
          nomeNuovaPersona +
          " " +
          cognomeNuovaPersona +
          "' esiste gia'!\nVuoi sovrascriverlo?";
        var mess = confirm(s, false, "Message");
        if (mess) {
          for (var i = lsAnagraficaPersone.items.length - 1; i > -1; i--) {
            var item = lsAnagraficaPersone.items[i];
            if (item.id === obj.id) {
              lsAnagraficaPersone.items.splice(i, 1);
            }
          }
          lsAnagraficaPersone.items.push(obj);
          localStorage.anagraficaPersone = JSON.stringify(lsAnagraficaPersone);
        } else {
          alert("Salvataggio interrotto!", "Attenzione");
        }
      }
    } else {
      alert("Il nome, cognome e codice fiscale non possono essere vuoti!");
    }
    loadListViewPersone();
  });

  // X TEST
  $("#btDettaglioPersonaEta").on("click", function() {
    var nomePalestraRifDett = $("#txtPalestraNomeRifDett").val();
    var nomePersona = $("#txtPersonaNome").val();
    var dataNascitaPersona = $("#txtPersonaDataNascita").val();

    if (dataNascitaPersona != "") {
      var elementiData = dataNascitaPersona.split("/");
      var giorno = Number(elementiData[0]);
      var mese = Number(elementiData[1]);
      var anno = Number(elementiData[2]);

      //alert(giorno + " - " + mese + " - " + anno);

      var diffAnno = Number(dataAnnoRif) - anno;
      var eta = diffAnno;
      console.log(mese + " - " + Number(dataMeseRif));

      if (mese < Number(dataMeseRif)) {
        eta = diffAnno - 1;
      } else if (mese == Number(dataMeseRif)) {
        if (giorno < Number(dataGiornoRif)) {
          eta = diffAnno - 1;
        }
      }
      console.log("età: " + eta);
      alert(eta + " anni");
    } else {
      alert("Il campo della data di nascita risulta vuoto!");
    }
  });

  $("#btDettaglioPersonaElimina").on("click", function() {
    var nomePalestraRifDett = $("#txtPalestraNomeRifDett").val();
    var idPalestraRifDett = $("#txtPalestraNomeRifDett").data("id");
    var id = $("#txtPersonaNome").data("id");
    var nomePersona = $("#txtPersonaNome").val();
    var cognomePersona = $("#txtPersonaCognome").val();
    var cfPersona = $("#txtPersonaCodiceFiscale").val();
    //console.log(nomePalestraRifDett + " > " + idPalestraRifDett);

    if (nomePersona !== "" && cognomePersona !== "" && cfPersona !== "") {
      var remove = false;

      // Lodash
      var indexVal = _.findIndex(lsAnagraficaPersone.items, function(item) {
        return item.id === id;
      });
      if (indexVal !== -1) {
        var s = "Confermi l'eliminazione?";
        var mess = confirm(s, false, "Message");
        if (mess) {
          remove = true;
          lsAnagraficaPersone.items.splice(indexVal, 1);
          localStorage.anagraficaPersone = JSON.stringify(lsAnagraficaPersone);

          if (localStorage.personeCorso === undefined) {
            localStorage.personeCorso = '{"items":[]}';
          }
          lsPersoneCorso = JSON.parse(localStorage.personeCorso);

          for (var i = lsPersoneCorso.items.length - 1; i > -1; i--) {
            var item = lsPersoneCorso.items[i];
            if (item.idPersona === id) {
              lsPersoneCorso.items.splice(i, 1);
              console.log(
                "Elimino l'abbinamento ai corsi di " + item.idPersona
              );
            }
          }
          localStorage.personeCorso = JSON.stringify(lsPersoneCorso);

          if (localStorage.iscrizioneAnnuale === undefined) {
            localStorage.iscrizioneAnnuale = '{"items":[]}';
          }
          lsIscrizioneAnnuale = JSON.parse(localStorage.iscrizioneAnnuale);

          for (var i = lsIscrizioneAnnuale.items.length - 1; i > -1; i--) {
            var item = lsIscrizioneAnnuale.items[i];
            if (item.idPersona === id) {
              lsIscrizioneAnnuale.items.splice(i, 1);
              console.log("Elimino l'iscrizione annuale di " + item.idPersona);
            }
          }
          localStorage.iscrizioneAnnuale = JSON.stringify(lsIscrizioneAnnuale);

          if (localStorage.iscrizioneMensile === undefined) {
            localStorage.iscrizioneMensile = '{"items":[]}';
          }
          lsIscrizioneMensile = JSON.parse(localStorage.iscrizioneMensile);

          for (var i = lsIscrizioneMensile.items.length - 1; i > -1; i--) {
            var item = lsIscrizioneMensile.items[i];
            if (item.idPersona === id) {
              lsIscrizioneMensile.items.splice(i, 1);
              console.log("Elimino le iscrizioni mensili di " + item.idPersona);
            }
          }
          localStorage.iscrizioneMensile = JSON.stringify(lsIscrizioneMensile);

          if (localStorage.certificatoMedicoAnnuale === undefined) {
            localStorage.certificatoMedicoAnnuale = '{"items":[]}';
          }
          lsCertificatoMedicoAnnuale = JSON.parse(
            localStorage.certificatoMedicoAnnuale
          );

          for (
            var i = lsCertificatoMedicoAnnuale.items.length - 1;
            i > -1;
            i--
          ) {
            var item = lsCertificatoMedicoAnnuale.items[i];
            if (item.idPersona === id) {
              lsCertificatoMedicoAnnuale.items.splice(i, 1);
              console.log("Elimino il certificato di " + item.idPersona);
            }
          }
          localStorage.certificatoMedicoAnnuale = JSON.stringify(
            lsCertificatoMedicoAnnuale
          );

          if (localStorage.presenze === undefined) {
            localStorage.presenze = '{"items":[]}';
          }
          lsPresenze = JSON.parse(localStorage.presenze);

          for (var i = lsPresenze.items.length - 1; i > -1; i--) {
            var item = lsPresenze.items[i];
            if (item.idPersona === id) {
              lsPresenze.items.splice(i, 1);
              console.log("Elimino le presenze di " + item.idPersona);
            }
          }
          localStorage.presenze = JSON.stringify(lsPresenze);
        }
      }
      if (!remove) {
        alert("La Persona da cancellare non e' presente!", "Attenzione");
      }

      $.mobile.changePage("#gestionePersonePage");
    } else {
      alert("Il nome, cognome e codice fiscale non possono essere vuoti!");
    }
    loadListViewPersone();
  });

  // Gestione stato persona
  $("#btDettaglioPersonaStato").on("click", function(e) {
    e.preventDefault();
    //alert("gestioneStatoPersona: " + gestioneStatoPersona);
    //$("#btDettaglioPersonaStato").css({"top":"1px","height":"29px","padding-top":"10px","width":"130px"});
    //$("#btDettaglioPersonaStato").css({"width":"130px"});
    console.log("prima: " + gestioneStatoPersona);
    if (gestioneStatoPersona == "Attivato") {
      $("#btDettaglioPersonaStato").text("Disattiva");
      gestioneStatoPersona = "Disattivato";
      console.log("dentro1: " + gestioneStatoPersona);
      $("#btDettaglioPersonaStato").text("Attiva");
    } else if (gestioneStatoPersona == "Disattivato") {
      $("#btDettaglioPersonaStato").text("Attiva");
      gestioneStatoPersona = "Attivato";
      console.log("dentro2: " + gestioneStatoPersona);
      $("#btDettaglioPersonaStato").text("Disattiva");
    }
    console.log("dopo: " + gestioneStatoPersona);
    //alert("Dopo:" + gestioneStatoPersona);

    loadListViewPersone();
  });

  // CORSI
  $("#btGestioneCorsi").on("click", function() {
    // Recupera i dati dalla pagina dettaglio palestra
    var nomePalestraRif = $("#txtPalestraNome").val();
    var idPalestraRif = $("#txtPalestraNome").data("id");

    // Inserisce i dati nella pagina gestione corsi
    $("#txtPalestraNomeRifCorsi").val(nomePalestraRif);
    $("#txtPalestraNomeRifCorsi").data("id", idPalestraRif);

    if (mostraPersoneDisattivate == "false") {
      $("#btDettaglioCorsoFiltra").text("Mostra iscritti disattivati");
    } else {
      $("#btDettaglioCorsoFiltra").text("Nascondi iscritti disattivati");
    }

    loadListViewCorsi();
    $.mobile.changePage("#gestioneCorsiPage");
  });

  $("#btGestioneCorsiAggiungi").on("click", function() {
    console.log("Creo un nuovo corso");
    var nomePalestraRifDett = $("#txtPalestraNomeRifCorsi").val();
    var idPalestraRifDett = $("#txtPalestraNomeRifCorsi").data("id");
    //console.log(nomePalestraRifDett + " > " + idPalestraRifDett);

    $("#dettaglioCorsoPage .resetta").val(""); // RESETTA
    $("#txtCorsoNome").data("id", ""); // RESETTA
    $("#txtPalestraNomeRifCorsoDett").val(nomePalestraRifDett);
    $("#txtPalestraNomeRifCorsoDett").data("id", idPalestraRifDett);
    //console.log($("#txtPalestraNomeRifCorsoDett").val() + " > " + $("#txtPalestraNomeRifCorsoDett").data("id"));

    $("#listaPersoneCorso")
      .empty()
      .listview(); // Aggiunto
    $.mobile.changePage("#dettaglioCorsoPage");
  });

  $("#btDettaglioCorsoSalva").on("click", function() {
    var nomePalestraRifDett = $("#txtPalestraNomeRifCorsoDett").val();
    var idPalestraRifDett = $("#txtPalestraNomeRifCorsoDett").data("id");
    var nomeNuovoCorso = $("#txtCorsoNome").val();
    //console.log(nomePalestraRifDett + " > " + idPalestraRifDett);

    if (nomeNuovoCorso !== "") {
      if (localStorage.anagraficaCorsi === undefined) {
        localStorage.anagraficaCorsi = '{"items":[]}';
      }
      lsAnagraficaCorsi = JSON.parse(localStorage.anagraficaCorsi);

      var obj = {
        id: idPalestraRifDett + "-" + nomeNuovoCorso,
        idCorso: nomeNuovoCorso,
        idPalestra: idPalestraRifDett,
        nomePalestra: nomePalestraRifDett,
        nome: nomeNuovoCorso,
        descrizione: $("#txtCorsoDescrizione").val(),
        costoMensileNumeri: $("#txtCorsoCostoMensileNumeri").val(),
        costoMensileLettere: $("#txtCorsoCostoMensileLettere").val(),
        nota: $("#txtCorsoNota").val()
      };

      var addItem = ckExistsItem(lsAnagraficaCorsi, obj.id);
      if (addItem) {
        lsAnagraficaCorsi.items.push(obj);
        localStorage.anagraficaCorsi = JSON.stringify(lsAnagraficaCorsi);
        alert(
          "Salvataggio completato!\nPrima di abbinare gli iscritti al corso, torna indietro nell'elenco dei corsi e rientra!",
          "Messaggio"
        );
      } else {
        var s =
          "Nella palestra '" +
          nomePalestraRifDett +
          "'\n'" +
          nomeNuovoCorso +
          "' esiste gia'!\nVuoi sovrascriverlo?";
        var mess = confirm(s, false, "Message");
        if (mess) {
          for (var i = lsAnagraficaCorsi.items.length - 1; i > -1; i--) {
            var item = lsAnagraficaCorsi.items[i];
            if (item.id === obj.id) {
              lsAnagraficaCorsi.items.splice(i, 1);
            }
          }
          lsAnagraficaCorsi.items.push(obj);
          localStorage.anagraficaCorsi = JSON.stringify(lsAnagraficaCorsi);
        } else {
          alert("Salvataggio interrotto!", "Attenzione");
        }
      }
    } else {
      alert("Campo nome vuoto!");
    }

    loadListViewCorsi();
  });

  $("#btDettaglioCorsoElimina").on("click", function() {
    var nomePalestraRifDett = $("#txtPalestraNomeRifCorsoDett").val();
    var idPalestraRifDett = $("#txtPalestraNomeRifCorsoDett").data("id");
    var id = $("#txtCorsoNome").data("id");
    var nomeCorso = $("#txtCorsoNome").val();
    if (nomeCorso !== "") {
      var remove = false;

      lsAnagraficaCorsi = JSON.parse(localStorage.anagraficaCorsi);
      // Lodash
      var indexVal = _.findIndex(lsAnagraficaCorsi.items, function(item) {
        return item.id === id;
      });
      if (indexVal !== -1) {
        var s = "Confermi l'eliminazione?";
        var mess = confirm(s, false, "Message");
        if (mess) {
          remove = true;
          lsAnagraficaCorsi.items.splice(indexVal, 1);
          localStorage.anagraficaCorsi = JSON.stringify(lsAnagraficaCorsi);

          if (localStorage.personeCorso === undefined) {
            localStorage.personeCorso = '{"items":[]}';
          }
          lsPersoneCorso = JSON.parse(localStorage.personeCorso);

          for (var i = lsPersoneCorso.items.length - 1; i > -1; i--) {
            var item = lsPersoneCorso.items[i];
            if (item.id === id) {
              lsPersoneCorso.items.splice(i, 1);
            }
          }
          localStorage.personeCorso = JSON.stringify(lsPersoneCorso);

          if (localStorage.iscrizioneAnnuale === undefined) {
            localStorage.iscrizioneAnnuale = '{"items":[]}';
          }
          lsIscrizioneAnnuale = JSON.parse(localStorage.iscrizioneAnnuale);

          for (var i = lsIscrizioneAnnuale.items.length - 1; i > -1; i--) {
            var item = lsIscrizioneAnnuale.items[i];
            if (item.idCorso === id) {
              lsIscrizioneAnnuale.items.splice(i, 1);
            }
          }
          localStorage.iscrizioneAnnuale = JSON.stringify(lsIscrizioneAnnuale);

          if (localStorage.iscrizioneMensile === undefined) {
            localStorage.iscrizioneMensile = '{"items":[]}';
          }
          lsIscrizioneMensile = JSON.parse(localStorage.iscrizioneMensile);

          for (var i = lsIscrizioneMensile.items.length - 1; i > -1; i--) {
            var item = lsIscrizioneMensile.items[i];
            if (item.idCorso === id) {
              lsIscrizioneMensile.items.splice(i, 1);
            }
          }
          localStorage.iscrizioneMensile = JSON.stringify(lsIscrizioneMensile);

          if (localStorage.certificatoMedicoAnnuale === undefined) {
            localStorage.certificatoMedicoAnnuale = '{"items":[]}';
          }
          lsCertificatoMedicoAnnuale = JSON.parse(
            localStorage.certificatoMedicoAnnuale
          );

          for (
            var i = lsCertificatoMedicoAnnuale.items.length - 1;
            i > -1;
            i--
          ) {
            var item = lsCertificatoMedicoAnnuale.items[i];
            if (item.idCorso === id) {
              lsCertificatoMedicoAnnuale.items.splice(i, 1);
            }
          }
          localStorage.certificatoMedicoAnnuale = JSON.stringify(
            lsCertificatoMedicoAnnuale
          );

          if (localStorage.presenze === undefined) {
            localStorage.presenze = '{"items":[]}';
          }
          lsPresenze = JSON.parse(localStorage.presenze);

          for (var i = lsPresenze.items.length - 1; i > -1; i--) {
            var item = lsPresenze.items[i];
            if (item.idCorso === id) {
              lsPresenze.items.splice(i, 1);
            }
          }
          localStorage.presenze = JSON.stringify(lsPresenze);
        }
      }
      if (!remove) {
        alert("Il Corso da cancellare non e' presente!", "Attenzione");
      }

      $.mobile.changePage("#gestioneCorsiPage");
    } else {
      alert("Campo nome vuoto!");
    }
    loadListViewCorsi();
  });

  $("#btDettaglioCorsoCategorie").on("click", function() {
    // Recupera i dati dalla pagina dettaglio corso
    var nomePalestraRif = $("#txtPalestraNomeRifCorsoDett").val();
    var idPalestraRif = $("#txtPalestraNomeRifCorsoDett").data("id");
    var nomeCorsoRif = $("#txtCorsoNome").val();

    // Inserisce i dati nella pagina gestione persone corso
    $("#txtPalestraNomeRifPersCorsoCategorie").val(nomePalestraRif);
    $("#txtPalestraNomeRifPersCorsoCategorie").data("id", idPalestraRif);
    $("#txtCorsoNomeRifPersCorsoCategorie").val(nomeCorsoRif);

    loadListViewPersoneCorsoCategorie();
    $.mobile.changePage("#gestionePersoneCorsoCategoriePage");
  });

  $("#listaCategorie").on("click", "input.btCatDelete", function() {
    var thisParent = $(this).closest(".listaCategorieTableRow");
    var id = thisParent.data("id");
    var nomeCat = thisParent.find(".txtCategoriaNome").val();
    var gradoCat = thisParent.find(".txtCategoriaGrado").val();
    console.log(id + " - " + nomeCat + " - " + gradoCat);

    // Lodash
    var indexVal = _.findIndex(lsCategorie.items, function(item) {
      return item.id === id;
    });
    if (indexVal !== -1) {
      var s = "Confermi l'eliminazione?";
      var mess = confirm(s, false, "Message");
      if (mess) {
        lsCategorie.items.splice(indexVal, 1);
        localStorage.categorie = JSON.stringify(lsCategorie);
        console.log("Elimino la categoria " + id);
        thisParent.remove();
      }
    }
    //$("#listaCategorie").listview("refresh");
  });

  $("#btElencoPersoneCorsoAggiungiSopra").on("click", aggiungiIscrittoAlCorso);
  $("#btElencoPersoneCorsoAggiungiSotto").on("click", aggiungiIscrittoAlCorso);

  $("#btGestionePersoneCorsoCategorieBack").on("click", function() {
    $.mobile.changePage("#dettaglioCorsoPage");
  });

  $("#btGestionePersoneCorsoCategorieInviaReport").on("click", function() {
    var datiCategorie = $("#contenitoreCategorie").data("datiCategorieTot");
    console.log(datiCategorie);

    var arrRows = [
      "palestraNome;corsoNome;categoriaNome;categoriaGrado;personaNome;personaCognome;personaGrado"
    ];
    for (var r = 0; r < datiCategorie.length; r++) {
      var row = datiCategorie[r];
      var str =
        row.palestraNome +
        ";" +
        row.corsoNome +
        ";" +
        row.categoriaNome +
        ";" +
        row.categoriaGrado +
        ";" +
        row.personaNome +
        ";" +
        row.personaCognome +
        ";" +
        row.personaGrado;
      arrRows.push(str);
    }

    var filePath = appSupportReportFolder + "/" + "ReportCategorie.csv";
    var contents = arrRows.join("\n");

    $.dropbox.basic.addFileText(filePath, contents);
    console.log(filePath);
    alert("Report categorie creato con successo!", ";Messaggio");
  });

  $("#btGestionePersoneAggiungiBackSopra").on(
    "click",
    aggiungiIscrittoAlCorsoConferma
  );
  $("#btGestionePersoneAggiungiBackSotto").on(
    "click",
    aggiungiIscrittoAlCorsoConferma
  );

  //Gestione creazione dichiarazione
  $("#listaPersoneCorso").on(
    "click",
    ".showPersonaNome input[type=button]",
    function() {
      switch (String(dataMeseRif)) {
        case "01":
          var monthLetterFormat = "Gennaio";
          break;
        case "02":
          var monthLetterFormat = "Febbraio";
          break;
        case "03":
          var monthLetterFormat = "Marzo";
          break;
        case "04":
          var monthLetterFormat = "Aprile";
          break;
        case "05":
          var monthLetterFormat = "Maggio";
          break;
        case "06":
          var monthLetterFormat = "Giugno";
          break;
        case "07":
          var monthLetterFormat = "Luglio";
          break;
        case "08":
          var monthLetterFormat = "Agosto";
          break;
        case "09":
          var monthLetterFormat = "Settembre";
          break;
        case "10":
          var monthLetterFormat = "Ottobre";
          break;
        case "11":
          var monthLetterFormat = "Novembre";
          break;
        case "12":
          var monthLetterFormat = "Dicembre";
          break;
      }

      var idBt = $(this).attr("id");
      var idPers = String(idBt).split("_")[1];

      var costoMensileNumeri = $("#txtCorsoCostoMensileNumeri").val();
      var costoMensileLettere = $("#txtCorsoCostoMensileLettere").val();

      console.log(idPers + " - " + idBt);

      lsAnagraficaPersone = JSON.parse(localStorage.anagraficaPersone);
      // Lodash
      var objVal = _.find(lsAnagraficaPersone.items, function(items) {
        return items.idPersona === idPers;
      });
      console.log(objVal);

      if (objVal) {
        lsAnagraficaPalestre = JSON.parse(localStorage.anagraficaPalestre);
        // Lodash
        var objValPal = _.find(lsAnagraficaPalestre.items, function(items) {
          return items.id === objVal.idPalestra;
        });
        console.log(objValPal);

        //alert("Creo la dichiarazione di " + objVal.nome + " " + objVal.cognome + "\ndella palestra " + objValPal.nome);

        var fileName =
          objValPal.nome +
          " - " +
          monthLetterFormat +
          " - " +
          dataAnnoRif +
          " - " +
          objVal.nome +
          " " +
          objVal.cognome +
          ".htm";
        var filePath = appSupportDichiarazioniFolder + "/" + fileName;

        var contents =
          "<html>" +
          "<header>" +
          '<link type="text/css" rel="stylesheet" href="css/style.css"/>' +
          "</header>" +
          "<body>" +
          '<div id="dichiarazione">' +
          '<div id="dichNomePal">' +
          objValPal.nome +
          "</div>" +
          '<div class="dichDatoPal">' +
          objValPal.via +
          ", " +
          objValPal.citta +
          "</div>" +
          '<div class="dichDatoPal">C.F. ' +
          objValPal.codiceFiscale +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich1">RICEVUTA DI PAGAMENTO N°:</div>' +
          '<div class="dichDatoCampo1"><input type="text" class="dichInputNumerazione"/></div>' +
          '<div class="dichDatoVuoto1"></div>' +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich2">Si riceve da:</div>' +
          '<div class="dichDatoCampo2">' +
          objVal.nomeGenitore +
          " " +
          objVal.cognomeGenitore +
          "</div>" +
          '<div class="dichDatoVuoto2"></div>' +
          '<div class="dichDatoTestoPiccolo">' +
          "Cognome e Nome del Gernitore" +
          "</div>" +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoVuoto2"></div>' +
          '<div class="dichDatoCampo2">' +
          objVal.codiceFiscaleGenitore +
          "</div>" +
          '<div class="dichDatoVuoto2"></div>' +
          '<div class="dichDatoTestoPiccolo">' +
          "Codice Fiscale del Gernitore" +
          "</div>" +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich2">La somma di €:</div>' +
          // + '<div class="dichDatoCampo2">' + costoMensileNumeri + ' (' + costoMensileLettere + ')' + '</div>'
          '<input type="text" class="dichInputCosto" value="' +
          costoMensileNumeri +
          " (" +
          costoMensileLettere +
          ")" +
          '"/>' +
          '<div class="dichDatoVuoto2"></div>' +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich2">A titolo di:</div>' +
          '<div class="dichDatoCampo2 testoSx1 noBordo">Frazionamento abbonamento annuale relativo al</div>' +
          '<div class="dichDatoVuoto2"></div>' +
          "</div>" +
          '<div class="dichDatoRigaVuota2"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoVuoto2"></div>' +
          '<div class="dichDatoCampo3">' +
          '<div class="dichDatoSottoEtich1">' +
          "Mese di:" +
          '</div><input type="text" class="dichInputMese"  value="' +
          monthLetterFormat +
          " / " +
          dataAnnoRif +
          '"/>' +
          '<div class="dichDatoVuoto2"></div>' +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich3">Per partecipazione allattività Sportiva Dilettantistica</div>' +
          '<div class="dichDatoCampo4">Corso di Karate</div>' +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich2">Del proprio figlio/a:</div>' +
          '<div class="dichDatoCampo2">' +
          objVal.nome +
          " " +
          objVal.cognome +
          "</div>" +
          '<div class="dichDatoVuoto2"></div>' +
          '<div class="dichDatoTestoPiccolo">' +
          "Cognome e Nome" +
          "</div>" +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich2"></div>' +
          '<div class="dichDatoCampo5">' +
          '<div class="dichDatoSottoEtich2">' +
          "Nato a:" +
          "</div>" +
          '<div class="dichDatoSottoCampo2 testoSx">' +
          objVal.luogoNascita +
          "</div>" +
          '<div class="dichDatoSottoEtich3 testoDx">' +
          "il:" +
          "</div>" +
          '<div class="dichDatoSottoCampo3 testoSx">' +
          objVal.dataNascita +
          "</div>" +
          "</div>" +
          "</div>" +
          '<div class="dichDatoRigaVuota2"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich2"></div>' +
          '<div class="dichDatoCampo5">' +
          '<div class="dichDatoSottoEtich2">' +
          "Residente a:" +
          "</div>" +
          '<div class="dichDatoSottoCampo2 testoSx">' +
          objVal.citta +
          "</div>" +
          '<div class="dichDatoSottoEtich3 testoDx">' +
          "CAP:" +
          "</div>" +
          '<div class="dichDatoSottoCampo3 testoSx">' +
          objVal.cap +
          "</div>" +
          "</div>" +
          "</div>" +
          '<div class="dichDatoRigaVuota2"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich2"></div>' +
          '<div class="dichDatoCampo5">' +
          '<div class="dichDatoSottoEtich2">' +
          "Via:" +
          "</div>" +
          '<div class="dichDatoSottoCampo2 testoSx">' +
          objVal.via +
          "</div>" +
          "</div>" +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich2">Località</div>' +
          '<div class="dichDatoCampo2 testoSx">' +
          objValPal.citta +
          "</div>" +
          '<div class="dichDatoVuoto2"></div>' +
          "</div>" +
          '<div class="dichDatoRigaVuota2"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoEtich2">Data</div>' +
          // + '<div class="dichDatoCampo2 testoSx">' + dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif + '</div>'
          '<input type="text" class="dichInputData testoSx" value="' +
          dataGiornoRif +
          "/" +
          dataMeseRif +
          "/" +
          dataAnnoRif +
          '"/>' +
          '<div class="dichDatoVuoto2"></div>' +
          "</div>" +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDatoRigaVuota1"></div>' +
          '<div class="dichDato">' +
          '<div class="dichDatoVuoto3"></div>' +
          '<div class="dichDatoCampo6">' +
          '<div class="dichDatoSottoCampo4"></div>' +
          '<div class="dichDatoTestoPiccolo">' +
          "Per quietanza" +
          "</div>" +
          '<div class="dichDatoTestoPiccolo">' +
          "Timbro e Firma della Società Sportiva" +
          "</div>" +
          "</div>" +
          "</div>" +
          "</div>" +
          "</body>" +
          "</html>";

        $.dropbox.basic.addFileText(filePath, contents);
        console.log(filePath);
        alert(
          "Il file:\n" + fileName + "\ne' stato creato con successo!",
          "Messaggio"
        );
      }
    }
  );

  $("#listaPersoneCorso").on(
    "click",
    ".showPersonaNome td.ckTab div.btCustonCk",
    function() {
      //alert($(this).is(":checked"));//attr("id"));
      //console.log(idCorsoNomeRifCorsoDett);

      $(this).toggleClass("checkboxchecked");

      var idCk = $(this).attr("id");
      var idPers =
        idPalestraNomeRifCorsoDett + "-" + String(idCk).split("_")[1];
      var tipoCkTemp = String(idCk).split("_")[0];
      var tipoCk = String(tipoCkTemp).split("-")[1];

      console.log(
        "idPalestraNomeRifCorsoDett: " +
          idPalestraNomeRifCorsoDett +
          "\nidCorsoNomeRifCorsoDett: " +
          idCorsoNomeRifCorsoDett +
          "\nidCk: " +
          idCk +
          "\nidPers: " +
          idPers +
          "\ntipoCk: " +
          tipoCk
      );

      if ($(this).hasClass("checkboxchecked")) {
        console.log("aggiungo");
        if (tipoCk == "I") {
          var obj = {
            idCorso: idCorsoNomeRifCorsoDett,
            dataPagamento:
              dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif,
            idMese: dataMeseRif,
            idAnno: dataAnnoRif,
            //idPersona: idPalestraNomeRifCorsoDett + "-" + idPers,
            idPersona: idPers,
            idPalestra: idPalestraNomeRifCorsoDett,
            stagioneSportiva: periodoStagioneSportiva
          };
          lsIscrizioneAnnuale = JSON.parse(localStorage.iscrizioneAnnuale);
          lsIscrizioneAnnuale.items.push(obj);
          localStorage.iscrizioneAnnuale = JSON.stringify(lsIscrizioneAnnuale);
        } else if (tipoCk == "C") {
          var obj = {
            idCorso: idCorsoNomeRifCorsoDett,
            dataPagamento:
              dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif,
            idMese: dataMeseRif,
            idAnno: dataAnnoRif,
            //idPersona: idPalestraNomeRifCorsoDett + "-" + idPers,
            idPersona: idPers,
            idPalestra: idPalestraNomeRifCorsoDett,
            stagioneSportiva: periodoStagioneSportiva
          };
          lsCertificatoMedicoAnnuale = JSON.parse(
            localStorage.certificatoMedicoAnnuale
          );
          lsCertificatoMedicoAnnuale.items.push(obj);
          localStorage.certificatoMedicoAnnuale = JSON.stringify(
            lsCertificatoMedicoAnnuale
          );
        } else if (tipoCk == "M") {
          var obj = {
            idCorso: idCorsoNomeRifCorsoDett,
            dataPagamento:
              dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif,
            idMese: dataMeseRif,
            idAnno: dataAnnoRif,
            //idPersona: idPalestraNomeRifCorsoDett + "-" + idPers,
            idPersona: idPers,
            idPalestra: idPalestraNomeRifCorsoDett,
            stagioneSportiva: periodoStagioneSportiva
          };
          lsIscrizioneMensile = JSON.parse(localStorage.iscrizioneMensile);
          lsIscrizioneMensile.items.push(obj);
          localStorage.iscrizioneMensile = JSON.stringify(lsIscrizioneMensile);
        } else if (tipoCk == "P") {
          var obj = {
            idCorso: idCorsoNomeRifCorsoDett,
            dataPresenza: dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif,
            idGiorno: dataGiornoRif,
            idMese: dataMeseRif,
            idAnno: dataAnnoRif,
            //idPersona: idPalestraNomeRifCorsoDett + "-" + idPers,
            idPersona: idPers,
            idPalestra: idPalestraNomeRifCorsoDett,
            stagioneSportiva: periodoStagioneSportiva
          };
          lsPresenze = JSON.parse(localStorage.presenze);
          lsPresenze.items.push(obj);
          localStorage.presenze = JSON.stringify(lsPresenze);
        }
      } else {
        console.log("tolgo");
        if (tipoCk == "I") {
          lsIscrizioneAnnuale = JSON.parse(localStorage.iscrizioneAnnuale);
          // Lodash
          var indexVal = _.findIndex(lsIscrizioneAnnuale.items, function(item) {
            //console.log(item.idCorso + " - " +  idCorsoNomeRifCorsoDett + "\n\n" +  item.idPersona  + " - " +   idPers);
            return (
              item.idCorso === idCorsoNomeRifCorsoDett &&
              item.idPersona === idPers
            );
          });
          console.log(indexVal);
          if (indexVal !== -1) {
            console.log("già presente, o tolgo!");
            lsIscrizioneAnnuale.items.splice(indexVal, 1);
            localStorage.iscrizioneAnnuale = JSON.stringify(
              lsIscrizioneAnnuale
            );
          }
        } else if (tipoCk == "C") {
          lsCertificatoMedicoAnnuale = JSON.parse(
            localStorage.certificatoMedicoAnnuale
          );
          // Lodash
          var indexVal = _.findIndex(lsCertificatoMedicoAnnuale.items, function(
            item
          ) {
            return (
              item.idCorso === idCorsoNomeRifCorsoDett &&
              item.idPersona === idPers
            );
          });
          if (indexVal !== -1) {
            lsCertificatoMedicoAnnuale.items.splice(indexVal, 1);
            localStorage.certificatoMedicoAnnuale = JSON.stringify(
              lsCertificatoMedicoAnnuale
            );
          }
        } else if (tipoCk == "M") {
          lsIscrizioneMensile = JSON.parse(localStorage.iscrizioneMensile);
          // Lodash
          var indexVal = _.findIndex(lsIscrizioneMensile.items, function(item) {
            return (
              item.idCorso === idCorsoNomeRifCorsoDett &&
              item.idPersona === idPers
            );
          });
          if (indexVal !== -1) {
            lsIscrizioneMensile.items.splice(indexVal, 1);
            localStorage.iscrizioneMensile = JSON.stringify(
              lsIscrizioneMensile
            );
          }
        } else if (tipoCk == "P") {
          lsPresenze = JSON.parse(localStorage.presenze);
          // Lodash
          var indexVal = _.findIndex(lsPresenze.items, function(item) {
            return (
              item.idCorso === idCorsoNomeRifCorsoDett &&
              item.idPersona === idPers
            );
          });
          if (indexVal !== -1) {
            lsPresenze.items.splice(indexVal, 1);
            localStorage.presenze = JSON.stringify(lsPresenze);
          }
        }
      }
    }
  );
}); // FINE document *************************************************************************

/**
 *
 * Setup Section
 *
 */
function setupFileUpload() {
  $("#fileupload").change(function(event) {
    var file = _.get($("#fileupload"), "[0].files[0]");
    if (file !== undefined) {
      addFile(file);
    }
  });
}

function setupList() {
  $("ul#dropbox-files").off("click", "li.dropbox-file");
  $("ul#dropbox-files").empty();
  $("ul#dropbox-files").on("click", "li.dropbox-file", function(event) {
    var target = $(event.target);
    var data = target.data();
    getFile(data.path, data.mimeType).then(function(res) {
      console.log("Successfully Downloaded file " + res.file.name);
    });
  });
}

function hasCreds() {
  $(".has-token-indicator").show();
  $(".has-token-indicator").css("background-color: green");
  $(".creds-container").hide();
  $(".file-upload").show();
  $(".file-list").show();
  $.dropbox.setToken($("#token").val());
  //loadFiles();
}

function resetCreds() {
  $(".has-token-indicator").hide();
  $(".creds-container").show();
  $("#token").val("");
  $.dropbox.setToken("");
  localStorage.dropbox_access = undefined;
  $(".file-upload").hide();
  $(".file-list").hide();
}

function loadFiles() {
  setupList();
  console.log("load files");
  return getFiles().then(handelFiles);
}

function appendList(html) {
  $("ul#dropbox-files").append(html);
}

function contentsToList(contents) {
  var template = {
    li_file: _.template(
      '<li class="dropbox-file"><a id="<%- file_source %>" data-path="<%- file_path %>" data-is-dir="<%- is_dir %>" data-mime-type="<%- file_mime_type %>" ><%- file_source %></a></li>'
    ),
    li_dir: _.template(
      '<li class="dropbox-file"><a id="<%- file_source %>" data-path="<%- file_path %>" data-is-dir="<%- is_dir %>" ><%- file_source %></a></li>'
    )
  };
  var li_html = _.chain(contents)
    .filter(function(content) {
      return !content.is_dir;
    })
    .map(function(content) {
      if (content.is_dir) {
        return template.li_dir({
          file_source: content.path,
          file_path: content.path,
          is_dir: content.is_dir
        });
      } else {
        return template.li_file({
          file_source: content.path,
          file_path: content.path,
          file_mime_type: content.mime_type,
          is_dir: content.is_dir
        });
      }
    })
    .value();
  var html = _.reduce(
    li_html,
    function(html, li) {
      html += li + "\n";
      return html;
    },
    ""
  );

  return html;
}

function handelFiles(filesObj) {
  var defer = Q.defer();
  _.merge(f, filesObj.files);
  Q.allSettled(filesObj.promises).done(function(filesArr) {
    _.forEach(filesArr, function(filePromise) {
      handelFiles(filePromise.value);
    });
  });

  var html = contentsToList(filesObj.files);
  appendList(html);
  return defer.promise;
}

/**
 *
 * Dropbox API Section
 *
 */
function getFiles(path) {
  if (window.f === undefined) {
    window.f = [];
  } else if (path === undefined) {
    _.remove(window.f);
  }
  var defer = Q.defer();
  $.dropbox.basic
    .getFiles(path)
    .then(function(files) {
      // console.dir(files.contents);
      var promises = _.chain(files.contents)
        .filter(function(content) {
          return content.is_dir;
        })
        .map(function(content) {
          var p = getFiles(content.path);
          return p;
        })
        .value();
      var files = _.chain(files.contents)
        .filter(function(content) {
          return !content.is_dir;
        })
        .map(function(content) {
          return content;
        })
        .value();
      defer.resolve({
        files: files,
        promises: promises
      });
    })
    .fail(function(err) {
      console.error(err);
      defer.reject(err);
    });
  return defer.promise;
}

function getFile(path, mime_type) {
  var defer = Q.defer();
  $.dropbox.basic
    .getFile({
      path: path,
      mime_type: mime_type
    })
    .then(function(res) {
      if (res !== undefined) {
        var name = path.substring(1);
        var file = new File([res], name, {
          type: res.type
        });
        var url = URL.createObjectURL(file);
        defer.resolve({
          blob: res,
          file: file,
          url: url
        });
      } else {
        defer.reject(new Error("Result is undefined"));
      }
    })
    .fail(function(err) {
      console.error(err);
      defer.reject(err);
    });
  defer.promise.then(function(res) {
    console.log(res);
    if (res !== undefined && res.url !== undefined) {
      window.open(res.url);
    }
  });
  return defer.promise;
}

function addFile(file) {
  return $.dropbox.basic.putFile(file).then(function() {
    return loadFiles();
  });
}

/**
 * Auth Section
 *
 */
function getAccessCode() {
  $.dropboxAuth.getAccessCode($("#appKey").val());
}

function getAuth() {
  $.dropboxAuth
    .getAuth($("#accessCode").val(), $("#appKey").val(), $("#appSecret").val())
    .then(function(result) {
      console.dir(result);
      localStorage.dropbox_access = JSON.stringify(result);
      $("#token").val(result.access_token);
      hasCreds();
    })
    .fail(function(error) {
      console.error("Error Loading..." + error);
      console.dir(error);
      resetCreds();
      $("#accessCode").val("");
    });
}

// Attività aggiuntive su Dropbox (Disattivare questa parte e togliere il pulsante su index.html)
//***************************************************************

function setLocalStorageSetAuth() {
  //glucamarc
  //localStorage.dropbox_access = '{"access_token":"WBH5ZtnfGoYAAAAAAAACyxL9UAnbsabHnmXYMQ7QSGyuInc0PGDh98ujTd4T4p62","token_type":"bearer","uid":"105697059"}';

  //gi-mar
  localStorage.dropbox_access =
    '{"access_token":"JvWLKbclmaUAAAAAAACN-Ygw7U-DWSWuLUzfi3C07UWrkRBRVN8TkgYsZRMEFPAs","token_type":"bearer","uid":"178838095","account_id":"dbid:AABWhlB7T1i5RLNzOm_pSDCFwswEZL2eYk4"}';

  //johnny
  //localStorage.dropbox_access = '{"access_token":"0tmqMdO8CrAAAAAAAAAA6rOAhJBMADFvKmsGPwFJi4LOK7uUI92L-s3QORnVbeyK","token_type":"bearer","uid":"712690333","account_id":"dbid:AABps-ZMfozOxz-3EYQXTUbOGnu7JB1BRCU"}';

  //alert("lsAuth: " + localStorage.dropbox_access.access_token);
  alert(
    "Sono state memorizzate le impostazioni di Dropbox!\nE' necessario riavviare l'app prima di procedere con le attivita'!"
  );
}

//CODICE *****************************************************

function ckExistsItem(ls, objId) {
  var add = true;
  // Lodash
  var objVal = _.find(ls.items, function(item) {
    //console.log(item.id + " - " + objId);
    return item.id === objId;
  });
  if (objVal) {
    add = false;
  }
  //console.log("add: " + add);
  return add;
}

//===============================================================

// FUNZIONI CONDIVISE DA PUSANTI
//===============================================================

function aggiungiPersona() {
  console.log("Creo una nuova iscrizione");
  var nomePalestraRifDett = $("#txtPalestraNomeRif").val();
  var idPalestraRifDett = $("#txtPalestraNomeRif").data("id");
  //console.log(nomePalestraRifDett + " > " + idPalestraRifDett);

  $("#dettaglioPersonaPage .resetta").val(""); // RESETTA
  $("#txtPersonaNome").data("id", ""); // RESETTA
  $("#txtPalestraNomeRifDett").val(nomePalestraRifDett);
  $("#txtPalestraNomeRifDett").data("id", idPalestraRifDett);
  //console.log($("#txtPalestraNomeRifDett").val() + " > " + $("#txtPalestraNomeRifDett").data("id"));
  gestioneStatoPersona = "Attivato";

  $("#dettaglioPersoneBack").attr("href", "#gestionePersonePage"); // Aggiunto per navigazione
  $.mobile.changePage("#dettaglioPersonaPage");
}

function aggiungiIscrittoAlCorso() {
  console.log("Entro aggiungiIscrittoAlCorso");
  // Recupera i dati dalla pagina dettaglio corso
  var nomePalestraRif = $("#txtPalestraNomeRifCorsoDett").val();
  var idPalestraRif = $("#txtPalestraNomeRifCorsoDett").data("id");
  var nomeCorsoRif = $("#txtCorsoNome").val();

  // Inserisce i dati nella pagina gestione persone corso
  $("#txtPalestraNomeRifPersCorsoAgg").val(nomePalestraRif);
  $("#txtPalestraNomeRifPersCorsoAgg").data("id", idPalestraRif);
  $("#txtCorsoNomeRifPersCorsoAgg").val(nomeCorsoRif);

  if (mostraPersoneDisattivate == "false") {
    $("#btPersoneCorsoAggFiltra").text("Mostra iscritti disattivati");
  } else {
    $("#btPersoneCorsoAggFiltra").text("Nascondi iscritti disattivati");
  }

  loadListViewPersoneCorsoAgg();
  $.mobile.changePage("#gestionePersoneCorsoAggPage");
}

function aggiungiIscrittoAlCorsoConferma() {
  //var nomePalestraRif = $("#txtPalestraNomeRifCorsoDett").val();
  var idPalestraRif = $("#txtPalestraNomeRifCorsoDett").data("id");
  //var nomeCorsoRif = $("#txtCorsoNome").val();
  var corso_id = $("#listaPersoneCorsoAgg").data("id");
  console.log("Confermo gli iscritti al corso: " + corso_id);

  lsPersoneCorso = JSON.parse(localStorage.personeCorso);

  var idPersoneNelCorso = $("#listaPersoneCorsoAgg input:checked");
  console.log(idPersoneNelCorso);

  for (var i = lsPersoneCorso.items.length - 1; i > -1; i--) {
    var item = lsPersoneCorso.items[i];
    //console.log(item.id + " - " + corso_id);
    if (item.id === corso_id) {
      lsPersoneCorso.items.splice(i, 1);
    }
  }

  for (var p = 0; p < idPersoneNelCorso.length; p++) {
    var persona_id = $(idPersoneNelCorso[p]).data("id");
    //console.log(corso_id + " > " + persona_id);
    var obj = {
      id: corso_id,
      idPersona: persona_id,
      idPalestra: idPalestraRif
    };
    lsPersoneCorso.items.push(obj);
  }

  localStorage.personeCorso = JSON.stringify(lsPersoneCorso);

  loadListViewPersoneCorso();
  $.mobile.changePage("#dettaglioCorsoPage");
}

//===============================================================

//LOAD LIST VIEW
//===============================================================

// CATEGORIE
function loadListViewCategorie() {
  console.log("Entro per creare la lista categorie");
  $("#listaCategorie")
    .empty()
    .listview();

  if (localStorage.categorie === undefined) {
    localStorage.categorie = '{"items":[]}';
  }
  lsCategorie = JSON.parse(localStorage.categorie);

  var itemsSort = _.sortBy(lsCategorie.items, "nome");
  //console.log(itemsSort);

  for (var i = 0; i < itemsSort.length; i++) {
    var item = itemsSort[i];
    if (item.nome !== "") {
      var $rowList = $(
        '<div class="ui-grid-d listaCategorieTableRow"><div class="ui-block-a"><input id="txtCategoriaNome' +
          i +
          '"class="txtCategoriaNome" type="text" value="' +
          item.nome +
          '"/></div>' +
          '<div class="ui-block-b"><input id="txtCategoriaGrado' +
          i +
          '" class="txtCategoriaGrado" type="text" value="' +
          item.grado +
          '"/></div>' +
          '<div class="ui-block-c"><input id="txtCategoriaEtaMin' +
          i +
          '" class="txtCategoriaEta" type="text" value="' +
          item.etaMin +
          '"/></div>' +
          '<div class="ui-block-d"><input id="txtCategoriaEtaMax' +
          i +
          '" class="txtCategoriaEta" type="text" value="' +
          item.etaMax +
          '"/></div><div class="ui-block-e"><input type="button" class="btCatDelete" value="Elimina" /></div></div>'
      ).data("id", item.id);

      //var $rowList = $('<div class="ui-grid-d listaCategorieTableRow"><div class="ui-block-a"><input class="txtCategoriaNome" type="text" value="' + item.nome + '"/></div>' + '<div class="ui-block-b"><input class="txtCategoriaGrado" type="text" value="' + item.grado + '"/></div>' + '<div class="ui-block-c"><input class="txtCategoriaEtaM" type="text" value="' + item.etaMin + '"/></div>' + '<div class="ui-block-d"><input class="txtCategoriaEta" type="text" value="' + item.etaMax + '"/></div><div class="ui-block-e"><input type="button" class="btCatDelete" value="Elimina" /></div></div>').data("id",item.id);
      $("#listaCategorie").append($rowList);
    }
  }

  $("#listaCategorie").listview("refresh");
}

//PALESTRE
function loadListViewPalestre() {
  console.log("Entro per creare la lista palestre");
  $("#listaPalestre")
    .empty()
    .listview();

  var itemsSort = _.sortBy(lsAnagraficaPalestre.items, "nome");
  //console.log(itemsSort);

  for (var i = 0; i < itemsSort.length; i++) {
    var item = itemsSort[i];
    var $rowList = $(
      '<li><a href="#" class="showPalestraNome">' + item.nome + "</a></li>"
    ).data("id", item.id);
    $("#listaPalestre").append($rowList);
  }

  $("#listaPalestre").listview("refresh");

  $(".showPalestraNome").on("click", function() {
    var id = $(this)
      .parent()
      .data("id");

    mostraDatiPalestra(id);
  });
}

function mostraDatiPalestra(id) {
  $("#dettaglioPalestraPage .resetta").val(""); // RESETTA

  // Lodash
  var indexVal = _.findIndex(lsAnagraficaPalestre.items, function(items) {
    //console.log(items.id + " - " + id);
    return items.id === id;
  });
  if (indexVal !== -1) {
    var vals = lsAnagraficaPalestre.items[indexVal];
    $("#txtPalestraNome").data("id", id);
    $("#txtPalestraNome").val(vals.nome);
    $("#txtPalestraVia").val(vals.via);
    $("#txtPalestraCap").val(vals.cap);
    $("#txtPalestraCitta").val(vals.citta);
    $("#txtPalestraProvincia").val(vals.provincia);
    $("#txtPalestraNazione").val(vals.nazione);
    $("#txtPalestraCodiceFiscale").val(vals.codiceFiscale);

    $.mobile.changePage("#dettaglioPalestraPage");
  }
}

//PERSONE
function loadListViewPersone() {
  console.log("Entro per creare la lista persone");

  var nomePal = $("#txtPalestraNomeRif").val();
  var idPal = $("#txtPalestraNomeRif").data("id");
  //console.log(nomePal + " > " + idPal);

  if (localStorage.anagraficaPersone === undefined) {
    localStorage.anagraficaPersone = '{"items":[]}';
  }

  var arrObjPersonePal = [];

  lsAnagraficaPersone = JSON.parse(localStorage.anagraficaPersone);
  for (var i = 0; i < lsAnagraficaPersone.items.length; i++) {
    var objPersona = lsAnagraficaPersone.items[i];

    if (objPersona.idPalestra === idPal) {
      arrObjPersonePal.push(objPersona);
    }
  }

  $("#listaPersone")
    .empty()
    .listview();

  var itemsSort = _.sortBy(arrObjPersonePal, "cognome");
  console.log(itemsSort);

  for (var i = 0; i < itemsSort.length; i++) {
    var item = itemsSort[i];

    if (mostraPersoneDisattivate == "false") {
      if (item.stato == "Attivato") {
        var $rowList = $(
          '<li><a href="#" class="showPersonaNome">' +
            item.cognome +
            " " +
            item.nome +
            "</a></li>"
        ).data("id", item.id);
        $("#listaPersone").append($rowList);
      }
    } else {
      if (item.stato == "Attivato") {
        var $rowList = $(
          '<li><a href="#" class="showPersonaNome">' +
            item.cognome +
            " " +
            item.nome +
            "</a></li>"
        ).data("id", item.id);
        $("#listaPersone").append($rowList);
      } else {
        var $rowList = $(
          '<li><a href="#" class="showPersonaNomeDisattivata">' +
            item.cognome +
            " " +
            item.nome +
            "</a></li>"
        ).data("id", item.id);
        $("#listaPersone").append($rowList);
      }
    }
  }

  $("#listaPersone").listview("refresh");

  $(".showPersonaNome").on("click", function() {
    var id = $(this)
      .parent()
      .data("id");

    mostraDatiPersona(id, "gestionePersonePage");
  });

  $(".showPersonaNomeDisattivata").on("click", function() {
    var id = $(this)
      .parent()
      .data("id");

    mostraDatiPersona(id, "gestionePersonePage");
  });
}

function mostraDatiPersona(id, nomePage) {
  $("#txtPalestraNomeRifDett").data("id", ""); // RESETTA
  $("#txtPersonaNome").data("id", ""); // RESETTA
  $("#dettaglioPersonaPage .resetta").val(""); // RESETTA

  var nomePalestraRifDett = $("#txtPalestraNomeRif").val();
  var idPalestraRifDett = $("#txtPalestraNomeRif").data("id");
  console.log(nomePalestraRifDett + " > " + idPalestraRifDett);

  $("#txtPalestraNomeRifDett").val(nomePalestraRifDett);
  $("#txtPalestraNomeRifDett").data("id", idPalestraRifDett);

  // Lodash
  var indexVal = _.findIndex(lsAnagraficaPersone.items, function(items) {
    //console.log(items.id + " - " + id);
    return items.id === id;
  });
  if (indexVal !== -1) {
    var vals = lsAnagraficaPersone.items[indexVal];

    $("#txtPersonaNome")
      .val(vals.nome)
      .data("id", vals.id);
    $("#txtPersonaCognome").val(vals.cognome);
    $("#txtPersonaDataNascita").val(vals.dataNascita);
    $("#txtPersonaLuogoNascita").val(vals.luogoNascita);
    $("#txtPersonaCodiceFiscale").val(vals.codiceFiscale);
    $("#txtPersonaIndirizzoVia").val(vals.via);
    $("#txtPersonaIndirizzoCap").val(vals.cap);
    $("#txtPersonaIndirizzoCitta").val(vals.citta);
    $("#txtPersonaIndirizzoProvincia").val(vals.provincia);
    $("#txtPersonaIndirizzoNazione").val(vals.nazione);
    $("#txtPersonaTelefono").val(vals.telefono);
    $("#txtPersonaCellulare").val(vals.cellulare);
    $("#txtPersonaEmail").val(vals.email);
    $("#txtPersonaGenitoreNome").val(vals.nomeGenitore);
    $("#txtPersonaGenitoreCognome").val(vals.cognomeGenitore);
    $("#txtPersonaGenitoreCodiceFiscale").val(vals.codiceFiscaleGenitore);
    $("#txtPersonaDataUltimoEsame").val(vals.dataUltimoEsame);
    $("#txtPersonaGrado").val(vals.grado);
    $("#txtPersonaNota").val(vals.nota);

    var stato = vals.stato;

    gestioneStatoPersona = stato;
    console.log(
      "Entro: " +
        vals.id +
        " : " +
        vals.nome +
        " " +
        vals.cognome +
        " > " +
        gestioneStatoPersona
    );

    if (nomePage == "gestionePersonePage") {
      $("#dettaglioPersoneBack").attr("href", "#gestionePersonePage");
    } else {
      $("#dettaglioPersoneBack").attr("href", "#dettaglioGruppoPage");
    }

    // Gestione stato persona
    if (stato == "Attivato") {
      $("#btDettaglioPersonaStato").text("Disattiva");
    } else {
      $("#btDettaglioPersonaStato").text("Attiva");
    }
    gestioneStatoPersona = stato;

    $.mobile.changePage("#dettaglioPersonaPage");
  }
}

//CORSI
function loadListViewCorsi() {
  console.log("Entro per creare la lista corsi");

  var nomePal = $("#txtPalestraNomeRifCorsi").val();
  var idPal = $("#txtPalestraNomeRifCorsi").data("id");

  //console.log(nomePal + " > " + idPal);

  if (localStorage.anagraficaCorsi === undefined) {
    localStorage.anagraficaCorsi = '{"items":[]}';
  }

  var arrObjCorsiPal = [];

  lsAnagraficaCorsi = JSON.parse(localStorage.anagraficaCorsi);
  for (var i = 0; i < lsAnagraficaCorsi.items.length; i++) {
    var objCorso = lsAnagraficaCorsi.items[i];

    if (objCorso.idPalestra === idPal) {
      arrObjCorsiPal.push(objCorso);
    }
  }

  $("#listaCorsi")
    .empty()
    .listview();

  var itemsSort = _.sortBy(arrObjCorsiPal, "nome");
  console.log(itemsSort);

  for (var i = 0; i < itemsSort.length; i++) {
    var item = itemsSort[i];

    var $rowList = $(
      '<li><a href="#" class="showCorsoNome">' + item.nome + "</a></li>"
    ).data("id", item.id);
    $("#listaCorsi").append($rowList);
  }

  $("#listaCorsi").listview("refresh");

  $(".showCorsoNome").on("click", function() {
    var id = $(this)
      .parent()
      .data("id");

    mostraDatiCorso(id, nomePal, idPal);
  });
}

function mostraDatiCorso(id, nomePal, idPal) {
  $("#txtPalestraNomeRifCorsoDett").data("id", ""); // RESETTA
  $("#txtCorsoNome").data("id", ""); // RESETTA
  $("#dettaglioCorsoPage .resetta").val(""); // RESETTA

  // Lodash
  var indexVal = _.findIndex(lsAnagraficaCorsi.items, function(items) {
    //console.log(items.id + " - " + id);
    return items.id === id;
  });
  if (indexVal !== -1) {
    var vals = lsAnagraficaCorsi.items[indexVal];

    $("#txtPalestraNomeRifCorsoDett").val(nomePal);
    $("#txtPalestraNomeRifCorsoDett").data("id", idPal);
    console.log(
      $("#txtPalestraNomeRifCorsoDett").val() +
        " > " +
        $("#txtPalestraNomeRifCorsoDett").data("id")
    );

    $("#txtCorsoNome")
      .val(vals.nome)
      .data("id", vals.id);
    $("#txtCorsoDescrizione").val(vals.descrizione);
    $("#txtCorsoCostoMensileNumeri").val(vals.costoMensileNumeri);
    $("#txtCorsoCostoMensileLettere").val(vals.costoMensileLettere);
    $("#txtCorsoNota").val(vals.nota);

    console.log("Entro: " + vals.id + " : " + vals.nome);

    loadListViewPersoneCorso();
    $.mobile.changePage("#dettaglioCorsoPage");
  }
}

function loadListViewPersoneCorso() {
  console.log("Entro per creare la lista persone per corso");
  idPalestraNomeRifCorsoDett = $("#txtPalestraNomeRifCorsoDett").data("id");
  idCorsoNomeRifCorsoDett = $("#txtCorsoNome").data("id");
  var dataRif = dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif;
  console.log("DataRif: " + dataRif);

  var arrIscrizioniAnnuali = [];
  var arrCertificatiMediciAnnuali = [];
  var arrIscrizioniMensili = [];
  var arrPresenze = [];

  if (localStorage.personeCorso === undefined) {
    localStorage.personeCorso = '{"items":[]}';
  }
  lsPersoneCorso = JSON.parse(localStorage.personeCorso);
  var arrIdPersoneCorso = [];
  for (var i = 0; i < lsPersoneCorso.items.length; i++) {
    var objPersonaCorso = lsPersoneCorso.items[i];
    if (objPersonaCorso.id === idCorsoNomeRifCorsoDett) {
      arrIdPersoneCorso.push(objPersonaCorso);
    }
  }
  console.log(arrIdPersoneCorso);

  if (localStorage.iscrizioneAnnuale === undefined) {
    localStorage.iscrizioneAnnuale = '{"items":[]}';
  }
  lsIscrizioneAnnuale = JSON.parse(localStorage.iscrizioneAnnuale);

  if (localStorage.iscrizioneMensile === undefined) {
    localStorage.iscrizioneMensile = '{"items":[]}';
  }
  lsIscrizioneMensile = JSON.parse(localStorage.iscrizioneMensile);

  if (localStorage.certificatoMedicoAnnuale === undefined) {
    localStorage.certificatoMedicoAnnuale = '{"items":[]}';
  }
  lsCertificatoMedicoAnnuale = JSON.parse(
    localStorage.certificatoMedicoAnnuale
  );

  if (localStorage.presenze === undefined) {
    localStorage.presenze = '{"items":[]}';
  }
  lsPresenze = JSON.parse(localStorage.presenze);

  if (localStorage.anagraficaPersone === undefined) {
    localStorage.anagraficaPersone = '{"items":[]}';
  }
  lsAnagraficaPersone = JSON.parse(localStorage.anagraficaPersone);
  var arrObjPersonePal = [];
  for (var i = 0; i < lsAnagraficaPersone.items.length; i++) {
    var objPersona = lsAnagraficaPersone.items[i];
    if (objPersona.idPalestra === idPalestraNomeRifCorsoDett) {
      arrObjPersonePal.push(objPersona);
    }
  }

  var arrObjPersonePalCorso = [];
  for (var i = 0; i < arrObjPersonePal.length; i++) {
    var objPersona = arrObjPersonePal[i];

    var objVal = _.find(arrIdPersoneCorso, function(item) {
      //console.log(item.idPersona + " - " + objPersona.id);
      return item.idPersona === objPersona.id;
    });
    if (objVal) {
      arrObjPersonePalCorso.push(objPersona);
    }
  }

  $("#listaPersoneCorso")
    .empty()
    .listview();

  var itemsSort = _.sortBy(arrObjPersonePalCorso, "cognome");
  console.log(itemsSort);

  var tabPersone =
    '<table id="iscrizioniTab"><tr><td>Nome</td><td class="ckTab">Iscriz. annuale</td><td class="ckTab">Certif. medico</td><td class="ckTab">Iscriz. mensile</td><td class="ckTab">Presenza</td></tr>';

  for (var i = 0; i < itemsSort.length; i++) {
    var item = itemsSort[i];

    //$(".showPersonaNome").css({"font-size":"20px"});

    var checkedIscrizioneAnnuale = "";
    var checkedCertificatoMedicoAnnuale = "";
    var checkedIscrizioneMensile = "";
    var checkedPresenza = "";

    var disabledDichiarazione = " ";
    if (item.nomeGenitore === "" && item.cognomeGenitore === "") {
      disabledDichiarazione = " disabled='true'";
    }

    var hideDichiarazione = " ";
    if (localStorage.dropbox_access === undefined) {
      hideDichiarazione = " class='nascosto'";
    }

    // Lodash
    var objVal = _.find(lsIscrizioneAnnuale.items, function(items) {
      return (
        items.idCorso === idCorsoNomeRifCorsoDett &&
        items.idPersona === item.id &&
        items.stagioneSportiva === periodoStagioneSportiva
      );
    });
    //console.log(objVal);
    if (objVal) {
      checkedIscrizioneAnnuale = "checkboxchecked";
    }

    // Lodash
    var objVal = _.find(lsCertificatoMedicoAnnuale.items, function(items) {
      return (
        items.idCorso === idCorsoNomeRifCorsoDett &&
        items.idPersona === item.id &&
        items.stagioneSportiva === periodoStagioneSportiva
      );
    });
    //console.log(objVal);
    if (objVal) {
      checkedCertificatoMedicoAnnuale = "checkboxchecked";
    }

    // Lodash
    var objVal = _.find(lsIscrizioneMensile.items, function(items) {
      return (
        items.idCorso === idCorsoNomeRifCorsoDett &&
        items.idPersona === item.id &&
        items.stagioneSportiva === periodoStagioneSportiva &&
        items.idMese === dataMeseRif
      );
    });
    //console.log(objVal);
    if (objVal) {
      checkedIscrizioneMensile = "checkboxchecked";
    }

    // Lodash
    var objVal = _.find(lsPresenze.items, function(items) {
      return (
        items.idCorso === idCorsoNomeRifCorsoDett &&
        items.idPersona === item.id &&
        items.stagioneSportiva === periodoStagioneSportiva &&
        items.idMese === dataMeseRif &&
        items.dataPresenza === dataRif
      );
    });
    //console.log(objVal);
    if (objVal) {
      checkedPresenza = "checkboxchecked";
    }

    if (mostraPersoneDisattivate == "false") {
      if (item.stato == "Attivato") {
        tabPersone +=
          '<tr class="showPersonaNome"><td class="personaTabCorso">' +
          item.cognome +
          " " +
          item.nome +
          '</td><td class="ckTab"><div class="iscrizioneAnnuale btCustonCk ' +
          checkedIscrizioneAnnuale +
          '" id="checkbox-I_' +
          item.idPersona +
          '"></div></td><td class="ckTab"><div class="certificatoMedicoAnnuale btCustonCk ' +
          checkedCertificatoMedicoAnnuale +
          '" id="checkbox-C_' +
          item.idPersona +
          '"></div></td><td class="ckTab"><div class="iscrizioneMensile btCustonCk ' +
          checkedIscrizioneMensile +
          '" id="checkbox-M_' +
          item.idPersona +
          '"></div></td><td class="ckTab"><div class="presenza btCustonCk ' +
          checkedPresenza +
          '" id="checkbox-P_' +
          item.idPersona +
          '"></div></td><td' +
          hideDichiarazione +
          '><input type="button" class="btDich" id="btDich_' +
          item.idPersona +
          '" value="Crea dichiarazione "' +
          disabledDichiarazione +
          " /></td></tr>";
      }
    } else {
      if (item.stato == "Attivato") {
        tabPersone +=
          '<tr class="showPersonaNome"><td class="personaTabCorso">' +
          item.cognome +
          " " +
          item.nome +
          '</td><td class="ckTab"><div class="iscrizioneAnnuale btCustonCk ' +
          checkedIscrizioneAnnuale +
          '" id="checkbox-I_' +
          item.idPersona +
          '"></div></td><td class="ckTab"><div class="certificatoMedicoAnnuale btCustonCk ' +
          checkedCertificatoMedicoAnnuale +
          '" id="checkbox-C_' +
          item.idPersona +
          '"></div></td><td class="ckTab"><div class="iscrizioneMensile btCustonCk ' +
          checkedIscrizioneMensile +
          '" id="checkbox-M_' +
          item.idPersona +
          '"></div></td><td class="ckTab"><div class="presenza btCustonCk ' +
          checkedPresenza +
          '" id="checkbox-P_' +
          item.idPersona +
          '"></div></td><td' +
          hideDichiarazione +
          '><input type="button" class="btDich" id="btDich_' +
          item.idPersona +
          '" value="Crea dichiarazione "' +
          disabledDichiarazione +
          " /></td></tr>";
      } else {
        tabPersone +=
          '<tr class="showPersonaNomeDisattivata"><td class="personaTabCorso">' +
          item.cognome +
          " " +
          item.nome +
          '</td><td class="ckTab"><div class="iscrizioneAnnuale btCustonCk ' +
          checkedIscrizioneAnnuale +
          '" id="checkbox-I_' +
          item.idPersona +
          '"></div></td><td class="ckTab"><div class="certificatoMedicoAnnuale btCustonCk ' +
          checkedCertificatoMedicoAnnuale +
          '" id="checkbox-C_' +
          item.idPersona +
          '"></div></td><td class="ckTab"><div class="iscrizioneMensile btCustonCk ' +
          checkedIscrizioneMensile +
          '" id="checkbox-M_' +
          item.idPersona +
          '"></div></td><td class="ckTab"><div class="presenza btCustonCk ' +
          checkedPresenza +
          '" id="checkbox-P_' +
          item.idPersona +
          '"></div></td><td' +
          hideDichiarazione +
          '><input type="button" class="btDich" id="btDich_' +
          item.idPersona +
          '" value="Crea dichiarazione "' +
          disabledDichiarazione +
          " /></td></tr>";
      }
    }
  }

  tabPersone += "</table>";
  $("#listaPersoneCorso").append(tabPersone);
  $(
    ".showPersonaNome input.btDich,.showPersonaNomeDisattivata input.btDich"
  ).button();
  $("#listaPersoneCorso").listview("refresh");
}

function loadListViewPersoneCorsoCategorie() {
  console.log("Entro per creare la lista persone del corso per categoria");
  idPalestraNomeRifCorsoDett = $("#txtPalestraNomeRifCorsoDett").data("id");
  nomePalestraNomeRifCorsoDett = $("#txtPalestraNomeRifCorsoDett").val();
  idCorsoNomeRifCorsoDett = $("#txtCorsoNome").data("id");
  nomeCorsoNomeRifCorsoDett = $("#txtCorsoNome").val();
  //var dataRif = dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif;
  //console.log("DataRif: " + dataRif);

  $("#listaPersoneCorsoCategorie")
    .empty()
    .listview();

  var arrDatiPersona = [];
  lsPersoneCorso = JSON.parse(localStorage.personeCorso);
  lsAnagraficaPersone = JSON.parse(localStorage.anagraficaPersone);
  lsCategorie = JSON.parse(localStorage.categorie);

  for (var i = 0; i < lsPersoneCorso.items.length; i++) {
    var objPersonaCorso = lsPersoneCorso.items[i];
    if (objPersonaCorso.id === idCorsoNomeRifCorsoDett) {
      // Lodash
      var objVal = _.find(lsAnagraficaPersone.items, function(items) {
        return items.id === objPersonaCorso.idPersona;
      });
      if (objVal) {
        console.log(
          objVal.nome + " " + objVal.cognome + " > " + objVal.dataNascita
        );
        var dataNascitaPersona = objVal.dataNascita;
        if (dataNascitaPersona != "") {
          var elementiData = dataNascitaPersona.split("/");
          var giorno = Number(elementiData[0]);
          var mese = Number(elementiData[1]);
          var anno = Number(elementiData[2]);

          //alert(giorno + " - " + mese + " - " + anno);

          var diffAnno = Number(dataAnnoRif) - anno;
          var eta = diffAnno;
          //console.log(mese + " - " + Number(dataMeseRif));

          if (Number(dataMeseRif) < mese) {
            eta = diffAnno - 1;
          } else if (Number(dataMeseRif) === mese) {
            if (Number(dataGiornoRif) < giorno) {
              eta = diffAnno - 1;
            }
          }
          console.log(
            "eta: " +
              objVal.cognome +
              " " +
              objVal.nome +
              " > " +
              objVal.grado +
              " > " +
              eta
          );

          var datiPersona = {
            nome: objVal.nome,
            cognome: objVal.cognome,
            grado: objVal.grado,
            eta: eta
          };
          arrDatiPersona.push(datiPersona);
        } else {
          console.log(
            "Il campo della data di nascita di " +
              objVal.cognome +
              " " +
              objVal.nome +
              " risulta vuoto!"
          );
        }
      }
    }
  }

  var rowList = '<div id="contenitoreCategorie">';
  var arrCategorie = [];
  for (var x = 0; x < lsCategorie.items.length; x++) {
    var categoria = lsCategorie.items[x];
    var numeroPersone = 0;
    if (categoria.nome != "") {
      rowList +=
        '<div data-role="collapsible"><h2>' +
        categoria.nome +
        " - (" +
        categoria.grado +
        ")</h2>" +
        '<ul data-role="listview" data-filter="true" data-filter-theme="a" data-divider-theme="b">';

      for (var y = 0; y < arrDatiPersona.length; y++) {
        console.log(
          "Persona prima: " +
            arrDatiPersona[y].cognome +
            " " +
            arrDatiPersona[y].nome +
            " - " +
            arrDatiPersona[y].grado +
            " - " +
            arrDatiPersona[y].eta
        );
        if (
          arrDatiPersona[y].eta >= categoria.etaMin &&
          arrDatiPersona[y].eta <= categoria.etaMax &&
          categoria.grado.indexOf(arrDatiPersona[y].grado) > -1
        ) {
          console.log("Compreso nel range");
          rowList +=
            "<li>" +
            arrDatiPersona[y].cognome +
            " " +
            arrDatiPersona[y].nome +
            " - (" +
            arrDatiPersona[y].grado +
            ")</li>";
          numeroPersone++;

          var datiPersonaCategoria = {
            palestraNome: nomePalestraNomeRifCorsoDett,
            corsoNome: nomeCorsoNomeRifCorsoDett,
            categoriaNome: categoria.nome,
            categoriaGrado: categoria.grado,
            personaNome: arrDatiPersona[y].nome,
            personaCognome: arrDatiPersona[y].cognome,
            personaGrado: arrDatiPersona[y].grado
          };
          arrCategorie.push(datiPersonaCategoria);
        }
      }
      if (numeroPersone == 0) {
        rowList += "<li>Nessun iscritto!</li>";
        var datiPersonaCategoria = {
          palestraNome: nomePalestraNomeRifCorsoDett,
          corsoNome: nomeCorsoNomeRifCorsoDett,
          categoriaNome: categoria.nome,
          categoriaGrado: categoria.grado,
          personaNome: "",
          personaCognome: "",
          personaGrado: ""
        };
        arrCategorie.push(datiPersonaCategoria);
      }
      rowList += "</ul>" + "</div>";
      rowList +=
        '<div class="numeroPersoneCategoria">' +
        numeroPersone +
        " iscritti" +
        "</div>";
    }
  } // Fine ciclo results2
  rowList += "</div>";
  $("#listaPersoneCorsoCategorie").append(rowList);
  //$("#listaPersoneCorsoCategorie").collapsibleset("refresh");
  $("#contenitoreCategorie").data("datiCategorieTot", arrCategorie);
}

function loadListViewPersoneCorsoAgg() {
  console.log("Entro per creare la lista persone del corso per aggiungerle");

  var nomePal = $("#txtPalestraNomeRifCorsoDett").val();
  var idPal = $("#txtPalestraNomeRifCorsoDett").data("id");
  var idCorso = $("#txtCorsoNome").data("id");
  $("#listaPersoneCorsoAgg").data("id", idCorso);
  console.log(nomePal + " - " + idPal + " > " + idCorso);

  var arrObjPersonePal = [];
  lsAnagraficaPersone = JSON.parse(localStorage.anagraficaPersone);
  for (var i = 0; i < lsAnagraficaPersone.items.length; i++) {
    var objPersona = lsAnagraficaPersone.items[i];

    if (objPersona.idPalestra === idPal) {
      arrObjPersonePal.push(objPersona);
    }
  }

  var arrIdPersoneCorso = [];
  lsPersoneCorso = JSON.parse(localStorage.personeCorso);
  for (var i = 0; i < lsPersoneCorso.items.length; i++) {
    var objPersonaCorso = lsPersoneCorso.items[i];
    if (objPersonaCorso.id === idCorso) {
      arrIdPersoneCorso.push(objPersonaCorso);
    }
  }

  $("#listaPersoneCorsoAgg")
    .empty()
    .listview();
  var itemsSort = _.sortBy(arrObjPersonePal, "cognome");
  //console.log(itemsSort);

  for (var i = 0; i < itemsSort.length; i++) {
    var item = itemsSort[i];

    // Lodash
    var indexVal = _.findIndex(arrIdPersoneCorso, function(items) {
      //console.log(items.idPersona + " - " + item.id);
      return items.idPersona === item.id;
    });

    var checked = " ";
    if (indexVal !== -1) {
      checked = " checked='checked'";
    }

    if (mostraPersoneDisattivate == "false") {
      if (item.stato == "Attivato") {
        var $rowList = $(
          '<input type="checkbox"' +
            checked +
            ' id="checkbox-' +
            item.id +
            '" /><label  class="showPersonaNome" for="checkbox-' +
            item.id +
            '">' +
            item.cognome +
            " " +
            item.nome +
            "</label>"
        ).data("id", item.id);
        $("#listaPersoneCorsoAgg").append($rowList);
      }
    } else {
      if (item.stato == "Attivato") {
        var $rowList = $(
          '<input type="checkbox"' +
            checked +
            ' id="checkbox-' +
            item.id +
            '" /><label class="showPersonaNome"for="checkbox-' +
            item.id +
            '">' +
            item.cognome +
            " " +
            item.nome +
            "</label>"
        ).data("id", item.id);
        $("#listaPersoneCorsoAgg").append($rowList);
      } else {
        var $rowList = $(
          '<input type="checkbox"' +
            checked +
            ' id="checkbox-' +
            item.id +
            '" /><label class="showPersonaNomeDisattivata" for="checkbox-' +
            item.id +
            '">' +
            item.cognome +
            " " +
            item.nome +
            "</label>"
        ).data("id", item.id);
        $("#listaPersoneCorsoAgg").append($rowList);
      }
    }
  }

  $("#listaPersoneCorsoAgg input[type='checkbox']")
    .checkboxradio()
    .checkboxradio("refresh");
  //$("#listaPersoneCorsoAgg").listview("refresh");
}

// LS SALVATAGGIO

function scriviLSStagioneSportiva(annoInizio, annoFine) {
  console.log("scrivo stagioneSportiva: " + annoInizio + " - " + annoFine);
  periodoStagioneSportiva = annoInizio + " - " + annoFine;
  localStorage.stagioneSportiva = '{"items":[]}';
  lsStagioneSportiva = JSON.parse(localStorage.stagioneSportiva);

  var obj = {
    annoInizio: annoInizio,
    annoFine: annoFine
  };
  lsStagioneSportiva.items.push(obj);
  localStorage.stagioneSportiva = JSON.stringify(lsStagioneSportiva);

  $("#txtStagioneSportivaAnnoInizio").val(annoInizio);
  $("#txtStagioneSportivaAnnoFine").val(annoFine);
  $("#settingsStagioneSportiva").text(
    "Anno sportivo: " + annoInizio + " - " + annoFine
  );
  $("#btSettingsStagioneSportivaDettaglioCorso").text(
    annoInizio + " - " + annoFine
  );
}

function calcolaDataNow(dataRif) {
  dataGiornoRif = String(dataRif.getDate());
  if (dataGiornoRif.length == 1) {
    dataGiornoRif = "" + 0 + dataGiornoRif;
  }
  dataMeseRif = String(dataRif.getMonth() + 1);
  if (dataMeseRif.length == 1) {
    dataMeseRif = "" + 0 + dataMeseRif;
  }
  dataAnnoRif = "" + dataRif.getFullYear();
  //alert(dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif);
}

function calcolaDataMod(giorno, mese, anno) {
  dataGiornoRif = giorno;
  if (dataGiornoRif.length == 1) {
    dataGiornoRif = "" + 0 + dataGiornoRif;
  }
  dataMeseRif = mese;
  if (dataMeseRif.length == 1) {
    dataMeseRif = "" + 0 + dataMeseRif;
  }
  dataAnnoRif = "" + anno;
  //alert(dataGiornoRif + "/" + dataMeseRif + "/" + dataAnnoRif);
}
